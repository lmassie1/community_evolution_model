##README FOR GUT COMMUNITY MODEL##
Leanne Massie lm3015@ic.ac.uk

This model runs in python2 and R version 3.3.1 in a Linux environment.

The necessary python packages are:
	-csv
	-scipy
	-numpy
	-progressbar
	-random
	
The necessary R packages are:
	-ggplot2
	-reshape2
	-gridExtra
	-cowplot
	-multcomp

Before running anything the above packages must be installed. 
This folder structure of this workflow consists of:

Code
	Chemostat-Model
		analysis.R
		analysis2.R
		final-community-model.py
		plot-output.R
		run-model.py
	Scripts
		6 example scripts
Data
	large-model
		High
		  example output files
		Small
		  example output files
	med-model
		High
		  example output files
		Small
		  example output files
	small-model
		High
		  example output files
		Small
		  example output files
	Stats-files
		example files generated during analysis
	Results.csv
	SummaryStats.csv
	SummaryStats100.csv
Latex
	images
		glth-both-new.pdf
		sl12-pretty.pdf
	count.txt #word count for .tex file
	icldt.cls #latex template file
	icldt.dtx #latex template file
	icldt.ins #latex template file
	icldt.pdf #latex template file
	makebst.log #log file for bibliography style
	Massie_Leanne_CMEE_2016.pdf #compiled writeup
	thesis5.bib #bibliography file
	thesis5.tex #source code for final writeup
	vancouver.bst #bibliography style
README.txt (this file)


To run this full project a terminal should be opened in the folder and the three R files should be have their working directory set to the save location (Code/Chemostat-Model) before running.

The command 'python run-model.py' can then be used to generate all the data used for this project. This takes approximately 24 hours. Once all data has been generated the analysis files, analysis.R and analysis2.R should be sourced to give the statistical outputs. The final writeup can be found in the Latex folder along with the source code, bibliography and style files necessary to compile it.


