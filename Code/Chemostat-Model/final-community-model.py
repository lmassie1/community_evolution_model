from __future__ import division

"""script to generate the running script. To be run with python2. this version allows all
parameters to be inputted at the top of the script and have all dependancies calulated from that. 
Substrate_num must be less than or equal to 78."""
__author__ = 'Leanne Massie (lm3015imperial.ac.uk)'
__version__ = '10.2'

import scipy
import numpy
import random
import subprocess
import string
from pulp import *
import sys

random.seed(sys.argv[1])

size = int(sys.argv[2])

if size ==1:
	location = "small-model/Low/iter"+str(sys.argv[1])
	name2= "small-low-iter"+str(sys.argv[1])
	functional_groups=1
	bacteria_per_group=5
elif size ==2:
	location = "small-model/High/iter"+str(sys.argv[1])
	name2= "small-high-iter"+str(sys.argv[1])
	functional_groups=5
	bacteria_per_group=1
elif size ==3:
	location = "med-model/Low/iter"+str(sys.argv[1])
	name2= "med-low-iter"+str(sys.argv[1])
	functional_groups=2
	bacteria_per_group=12
elif size ==4:
	location = "med-model/High/iter"+str(sys.argv[1])
	name2= "med-high-iter"+str(sys.argv[1])
	functional_groups=12
	bacteria_per_group=2
elif size == 5:
	location = "large-model/Low/iter"+str(sys.argv[1])
	name2= "large-low-iter"+str(sys.argv[1])
	functional_groups=2
	bacteria_per_group=25
elif size ==6:
	location = "large-model/High/iter"+str(sys.argv[1])
	name2= "large-high-iter"+str(sys.argv[1])
	functional_groups=25
	bacteria_per_group=2

########################################
### DEFINE THE MODEL PARAMETERS HERE ###
########################################

###System details###
timelength=10000 #how many timesteps to interate through
dilution= 0.04 #what the inflow/outflow rate through the system is
timestep = 5 #how many timelengths should pass between sampling the system

###Substrate details###
substrate_num= 5 #must be less than or equal to 78
substrate_sep= 1 #what the inflow concentrations of substrates should be separated by
substrate_start= list((str(i+1) for i in range(substrate_num*int(substrate_sep)) if i % int(substrate_sep) == 0)) #the inflow concentration of substrates


###Bacteria details###
#~ functional_groups=25 #number of groups of bacteria with similar traits ie functional groups
#~ bacteria_per_group= 2 #number of bacteria in each of the functional groups
bacteria_num= functional_groups*bacteria_per_group
bacteria_start=1#starting concentration of bacteria
max_bacteria= 10 #maximum level that bacteria can reach

###Enzyme details###
enzyme_allocation= 1/substrate_num#the proportion of enzyme each bacteria allocates to digesting each substrate
conversion_vector=[0.8-(0.05*x)for x in range(substrate_num)]
###the above line generates a vector describing how much energy each substrate can provide
###to a bacteria for growth. These are the same for all bacteria and just determined
###by the substrate number.


###Toxicity/Antibiotic details###
#~ toxicity = [0 for x in range(bacteria_num)]# this turns off the toxin's effect
toxicity= [0.01+ random.uniform(-0.01,0.01) for x in range(bacteria_num)] #generates random toxicity values of the toxin to the various bacteria
toxin_time= (3000,5000) #the start and end times that the toxin should be in the system
toxin_inflow = 0.019
antitoxin = [0 for x in range(bacteria_num)] #starting values for antitoxin enzyme
#########################################################################
### GENERATES THE NAMES OF THE SUBSTRATES AND BACTERIAS FOR LATER USE ###
substrates="S1"
for i in range(1,substrate_num):
	substrates=substrates+",S"+str(i+1)

bacterias="N1"
for i in range(1,bacteria_num):
	bacterias=bacterias+",N"+str(i+1)
#########################################################################


### OPENS THE SCRIPT FILE AND WRITES THE NECESSARY IMPORTS AND ENZYME KINETICS INTO IT ###
writer = open("../Scripts/"+name2+".py", "w")
writer.write("#!/usr/bin/env python")
writer.write("\nfrom __future__ import division")
writer.write("\n\nimport csv")
writer.write("\nfrom pulp import *")
writer.write("\nimport scipy")
writer.write("\nimport numpy")
writer.write("\nfrom progressbar import ProgressBar")
writer.write("\nimport random")
writer.write("\nrandom.seed(sys.argv[1])")
writer.write("\n\ntimestep= "+ str(timestep)+" #how many timesteps should pass between sampling")
writer.write("\nglobal timelength")
writer.write("\ntimelength= "+str(timelength)+" #how many timesteps in total to run the model for")
writer.write("\ndilution= "+str(dilution))
writer.write("\nbacteria_num= "+str(bacteria_num))
writer.write("\nsubstrate_num="+str(substrate_num))
writer.write("\nsubstrate_start="+str(substrate_start))
writer.write("\nbacteria_start="+str(bacteria_start))
writer.write("\nfunctional_groups="+str(functional_groups))
writer.write("\nbacteria_per_group="+str(bacteria_per_group))
writer.write("\n\n# # S E T  U P  C O D E # #")
writer.write("\nkcat= [[0.1 for x in range(substrate_num+1)]for x in range(bacteria_num)]")
writer.write("\nkm= [[0.5 for x in range(substrate_num+1)]for x in range(bacteria_num)]")
#~ writer.write("\nconv= [[0.8 for x in range(substrate_num)]for x in range(bacteria_num)]")
writer.write("\nE= [["+str(enzyme_allocation)+" for x in range(substrate_num)]for x in range(bacteria_num)]")
writer.write("\ntox= "+str(toxicity))
writer.write("\ntoxin_time = "+str(toxin_time))
writer.write("\nantitoxin = "+str(antitoxin))
writer.write("\n\n\n#######################################")
writer.write("\n### GENERATES THE CONVERSION MATRIX ###")
writer.write("\nconv_groups = ["+str(conversion_vector)+" for x in range(bacteria_num)]")
writer.write("\nfor i in range(0,bacteria_num):")
writer.write("\n\trandom_num= [random.uniform(-0.05,0.05) for x in range(bacteria_num)]")
writer.write("\n\tfor j in range(0,substrate_num):")
writer.write("\n\t\tconv_groups[i][j]= conv_groups[i][j]+random_num[i]")
writer.write("\n\t\tif (conv_groups[i][j]<0):")
writer.write("\n\t\t\tconv_groups[i][j]=0")
writer.write("\n\t\tif (conv_groups[i][j]>1):")
writer.write("\n\t\t\tconv_groups[i][j]=1")
writer.write("\n\nconv=[[0 for x in range(substrate_num)]for x in range(bacteria_num)]")
writer.write("\nfor h in range(0,int(bacteria_per_group)):")
writer.write("\n\tfor i in range(0,bacteria_num):")
writer.write("\n\t\tfor j in range(0,substrate_num):")
writer.write("\n\t\t\tconv[i][j]=conv_groups[h][j]+random.uniform(-0.05,0.05)")
writer.write("\n\n#######################################")

### WRITES IN THE UPPER AND LOWER TOXIN CONCENTRATIONS ###
writer.write("\n\ntoxin_0=0")
writer.write("\ntoxin_1="+str(toxin_inflow))
writer.write("\ntoxin= toxin_0\n")


### WRITES IN THE STARTING SUBSTRATE CONCENTRATIONS ###
for i in range(0,substrate_num):
	writer.write("\nS"+str(i+1)+"_0="+str(substrate_start[i]))
	writer.write("\nS"+str(i+1)+"=S"+str(i+1)+"_0")

#########################################
writer.write("\n\n###FUNCTIONS###\n")####
#########################################

### GENERATES THE BACTERIA GROWTH EQUATIONS BASED ON THE NUMBER OF SUBSTRATES PRESENT ###
for i in range(0, bacteria_num):
	names = ""
	function="\ndef bacteria_"+str(i+1)+"(N"+str(i+1)+",kcat,conv,E,tox,dilution,toxin,antitoxin"
	for j in range(0, substrate_num):
		name1 = str('+(conv['+str(i)+']['+str(j)+']*(kcat['+str(i)+']['+str(j)+']*E['+str(i)+']['+str(j)+']*S'+str(j+1)+'/(km['+str(i)+']['+str(j)+']+S'+str(j+1)+'))*N'+str(i+1)+')')
		names = names + name1
		function1= ",S"+str(j+1)
		function = function+ function1
		
	writer.write("\n"+function+ "):")
	writer.write("\n\tBacteria"+str(i+1)+"= N"+str(i+1)+names+"-(dilution*N"+str(i+1)+")"+"-(N"+str(i+1)+"*tox["+str(i)+"]*toxin)+(kcat["+str(i)+"]["+str(j+1)+"]*antitoxin["+str(i)+"]*toxin/(km["+str(i)+"]["+str(j+1)+"]+toxin))")
	writer.write("\n\treturn (Bacteria"+str(i+1)+")")
	
	
### GENERATES THE CORRESPONDING SUBSTRATE CONCENTRATION CHANGE EQUATIONS BASED ON THE NUMBER OF BACTERIA ###
### FEEDING ON THEM ###
for i in range(0, substrate_num):
	subs = ""
	bacs="\ndef substrate_"+str(i+1)+"(S"+str(i+1)+",kcat,conv,E,dilution"
	for j in range(0, bacteria_num):
		subs1=(str( '-((kcat['+str(j)+']['+str(i)+']*E['+str(j)+']['+str(i)+']*S'+str(i+1)+'/(km['+str(j)+']['+str(i)+']+S'+str(i+1)+'))*N'+str(j+1)+')'))
		subs = subs + subs1
		bacs1= ",N"+str(j+1)
		bacs=bacs+bacs1
	
	all_bacs= bacs+"):"
	writer.write("\n"+all_bacs)
	writer.write('\n\tSubstrate'+str(i+1)+'=S'+ str(i+1)+subs+"-dilution*S"+str(i+1)+"+dilution*S"+str(i+1)+"_0")
	writer.write("\n\treturn (Substrate"+str(i+1)+")")

### WRITES IN THE TOXIN EQUATION ###
writer.write("\n\ndef toxin_eqn(toxin,toxin_1):")
writer.write("\n\ttoxin = toxin + toxin_1 - (toxin*dilution)")
writer.write("\n\treturn(toxin)")

################################


### WRITES THE MAIN RUNNING CODE TO THE SCRIPT INCLUDING HOW THE SCRIPT WRITES ITS OWN ###
### RESULTS FILES ###

writer.write("\n\n#RUNNING CODE#")
#~ writer.write("\n\nglobal timelength")
#~ writer.write("\ntimelength = "+str(timelength))
writer.write("\nsamplepoints= range(0,int(timelength)) #create a vector of the number")
writer.write("\n#of times you want to sample the population size")
writer.write("\npbar= ProgressBar(samplepoints)#setting up the progessbar based on number of sample points")
writer.write("\nresults_file = open('../../Data/"+location+"model_output.csv', 'w')")
writer.write("\nwriter = csv.writer(results_file, delimiter = \",\")")
writer.write("\nenzyme_file = open('../../Data/"+location+"enzyme_evolution.csv', 'w')")
writer.write("\nwriter2 = csv.writer(enzyme_file, delimiter = \",\")")


################################################################################
## THIS BLOCK OF CODE WRITES THE COLUMN HEADERS INTO THE RUNNING SCRIPT TO BE ##
## WRITTEN INTO ITS RESULTS FILE ##
headings="Time\""
results_row="i"
for i in range(0,bacteria_num):
	headings=headings+",\"Bacteria_"+str(i+1)+"\""
	results_row=results_row+ ",N"+str(i+1)
for i in range(0,substrate_num):
	headings=headings+", \"Substrate_"+str(i+1)+"\""	

headings=headings+", \"Toxin"
results_row=results_row+",toxin"

writer.write("\nwriter.writerow([\""+headings+"\"])")

enzyme_heading="Time\","
enzyme_row="i+1,"
for i  in range(0,bacteria_num):
	for j in range(0,substrate_num):
		enzyme_heading=enzyme_heading+"\"Bac"+str(i+1)+"_Sub"+str(j+1)+"\","
		enzyme_row=enzyme_row+"E["+str(i)+"]["+str(j)+"],"
enzyme_heading=enzyme_heading.strip(" ")
enzyme_heading=enzyme_heading.strip(",")
enzyme_row=enzyme_row.strip(" ")
enzyme_row=enzyme_row.strip(",")

writer.write("\nwriter2.writerow([\""+enzyme_heading+"])")


writer.write("\nantitoxin_file = open('../../Data/"+location+"antitoxin_evolution.csv','w')")
writer.write("\nwriter3= csv.writer(antitoxin_file, delimiter = \",\")")
writer.write("\nwriter3.writerow([\"Time\",\"variable\",\"value\"])")


##############################################################################


for i in range(0,bacteria_num):
	
	writer.write("\nN"+str(i+1)+"=bacteria_start")

writer.write("\nwriter.writerow([0,"+bacterias+","+substrates+",toxin])")
# WRITES IN THE INITIAL VALUES #


##########################################################################################
### THE START OF THE MAIN FOR LOOP THAT CALLS ALL THE FUNCTIONS OF BACTERIA, SUBSTRATE ###
### TOXIN AND UPDATES THE ENZYME ALLOCATION VALUES AT THE END OF EACH LOOP TO ADJUST TO ##
### THE AVAILABLE SUBSTRATES ##

writer.write("\n\n\nfor i in pbar(samplepoints):")

writer.write("\n\t#for each of the sample points call each bacterial equation once and write the output to the results file")
for i in range (0,bacteria_num):
	writer.write("\n\tbacteria_"+str(i+1)+"_output = bacteria_"+str(i+1)+"(N"+str(i+1)+",kcat,conv,E,tox,dilution,toxin,antitoxin,"+substrates+")")
	writer.write("\n\tif (bacteria_"+str(i+1)+"_output < 0.0001):")
	writer.write("\n\t\tN"+str(i+1)+"=0")
	
	writer.write("\n\telif (bacteria_"+str(i+1)+"_output >"+str(max_bacteria)+"):")
	writer.write("\n\t\tN"+str(i+1)+"="+str(max_bacteria))
	
	writer.write("\n\telse:")
	writer.write("\n\t\tN"+str(i+1)+" = bacteria_"+str(i+1)+"_output")
	writer.write("\n")
	
for i in range(0,substrate_num):
	writer.write("\n\tnew_substrate_"+str(i+1)+"= substrate_"+str(i+1)+"(S"+str(i+1)+",kcat,km,E,dilution,"+bacterias+")")
	writer.write("\n\tif (new_substrate_"+str(i+1)+" < 0.0001):")
	writer.write("\n\t\tS"+str(i+1)+"=0")
	writer.write("\n\telse:")
	writer.write("\n\t\tS"+str(i+1)+"=new_substrate_"+str(i+1))
	writer.write("\n\n")



#~ if len(toxin_time) >1 :
		#~ 
	#~ writer.write("\n\tif i in range"+str(toxin_time[0])+" or range"+str(toxin_time[1])+":")
	#~ 
#~ else:
writer.write("\n\tif i in range"+str(toxin_time)+":")


writer.write("\n\t\tnew_toxin=toxin_eqn(toxin,toxin_1)")
writer.write("\n\t\ttoxin= new_toxin")
writer.write("\n\n")
writer.write("\n\telse:")
writer.write("\n\t\tnew_toxin=toxin_eqn(toxin,toxin_0)")
writer.write("\n\t\ttoxin= new_toxin\n")

### SORTS THE SUBSTRATES BY CONCENTRATION TO ALLOW FOR MEANINGFUL EVOLUTION ###
sub_tuples="sub_tuples= [(\"S1\",S1)"
for i in range(1,substrate_num):
	sub_tuples=sub_tuples+ ",(\"S"+str(i+1)+"\",S"+str(i+1)+")"
	
sub_tuples=sub_tuples+"]"

writer.write("\n\tS=["+substrates+"] #forms a matrix of the substrate values at the end of that run")
writer.write("\n")

###################################################
writer.write("\n### THE ENZYME EVOLUTION CODE ###")
###################################################
letters = list(string.ascii_lowercase)
tabs_all =""
used_letters = "a"
tabs= ""
plussed_letters = "a"

#generates names of substrates up to 78 substrates
for i in range(1,substrate_num):
	if i< 26:
		used_letters= used_letters+letters[i]
		plussed_letters= plussed_letters+"+"+letters[i]
		tabs= tabs+ "\t"
		#~ writer.write("\n\t"+ tabs+"for "+letters[i]+" in numpy.arange(0,1,0.01):")
	elif 26<i<52:
		used_letters= used_letters+letters[i-26]+letters[i-26]
		plussed_letters= plussed_letters+"+"+letters[i-26]+letters[i-26]
		tabs= tabs+ "\t"
		#~ writer.write("\n\t"+ tabs+"for "+letters[i-26]+letters[i-26]+" in numpy.arange(0,1,0.01):")
	else:
		used_letters= used_letters+letters[i-52]+letters[i-52]+letters[i-52]
		plussed_letters= plussed_letters+"+"+letters[i-52]+letters[i-52]+letters[i-52]
		tabs= tabs+ "\t"
		#~ writer.write("\n\t"+ tabs+"for "+letters[i-52]+letters[i-52]+letters[i-52]+" in numpy.arange(0,1,0.01):")


writer.write("\n\n\tfor bacteria_ID in range(bacteria_num):")
writer.write("\n\t\tif (i%5 == 0):")
writer.write("\n\t\t\tN=["+bacterias+"] #forms a matrix of the substrate values at the end of that run")
writer.write("\n\t\t\tif N[bacteria_ID] > 0:")


writer.write("\n\t\t\t\tmu = [random.uniform(0, 0.02) for x in range(substrate_num)]")
writer.write("\n\t\t\t\tmut = random.uniform(0, 0.02)")


for i in range(substrate_num):
	writer.write("\n\t\t\t\t"+used_letters[i]+"=LpVariable(\'"+used_letters[i]+"\',0,1)")

writer.write("\n\t\t\t\tanti = LpVariable('anti',0,1)")
writer.write("\n\t\t\t\tprob = LpProblem(\"myProblem\",LpMaximize)")
writer.write("\n\t\t\t\tprob += "+plussed_letters+"+anti<=1 #the constraint that they must all sum to 1")

problem_text="prob+= "
for j in range(0, substrate_num):
	next_problem_text = str('(conv[bacteria_ID]['+str(j)+']*(kcat[bacteria_ID]['+str(j)+']*'+used_letters[j]+'*S'+str(j+1)+'/(km[bacteria_ID]['+str(j)+']+S'+str(j+1)+')))+')
	problem_text = problem_text+next_problem_text
problem_text= problem_text.strip("+")

writer.write("\n\t\t\t\t"+problem_text+"+(kcat[bacteria_ID]["+str(substrate_num)+"]*anti*toxin/(km[bacteria_ID]["+str(substrate_num)+"]+toxin))")
writer.write("\n\n\t\t\t\tstatus = prob.solve()")
writer.write("\n\t\t\t\tLpStatus[status]\n")

values_as_integers="["
for i in range(substrate_num):
	next_values_as_integers =  "int(value("+used_letters[i]+")),"
	values_as_integers=values_as_integers+ next_values_as_integers

values_as_integers= values_as_integers.strip(",")
values_as_integers= values_as_integers+"]"

writer.write("\n\t\t\t\tbest_enzyme= "+values_as_integers)
writer.write("\n\t\t\t\tE_diff = numpy.array(E[bacteria_ID])-numpy.array(best_enzyme)")
writer.write("\n\t\t\t\tE_diff_matrix = E_diff*mu")
writer.write("\n\t\t\t\tE[bacteria_ID] = E[bacteria_ID] - E_diff_matrix")

writer.write("\n\n\t\t\t\tbest_antitoxin=int(value(anti))")
writer.write("\n\t\t\t\tantitoxin_diff = best_antitoxin-antitoxin[bacteria_ID]")
writer.write("\n\t\t\t\tnew_antitoxin = antitoxin_diff*mut")
writer.write("\n\t\t\t\tantitoxin[bacteria_ID] = antitoxin[bacteria_ID]+ new_antitoxin")
writer.write("\n\t\t\t\twriter3.writerow([i+1,\"Bac\"+str(bacteria_ID+1),antitoxin[bacteria_ID]])")


writer.write("\n\n\t\t\telif N[bacteria_ID] == 0 :")
writer.write("\n\t\t\t\tE[bacteria_ID] = E[bacteria_ID] * 0")
writer.write("\n\t\t\t\tantitoxin[bacteria_ID] = antitoxin[bacteria_ID] * 0")

#####################################################


writer.write("\n\n\tif i % timestep == 0:")
writer.write("\n\t\twriter2.writerow(["+enzyme_row+"])")
writer.write("\n\t\twriter.writerow([i+1,"+bacterias+","+substrates+",toxin])")

writer.write("\n\nprint (\"Model run successfully\")")
### END OF MAIN FOR LOOP ###
#########################################################################################






writer.write("\n\nresults_file.close()")
writer.write("\nenzyme_file.close()")
writer.write("\nantitoxin_file.close()\n\n")




bac_tuples = "bac_tuples = [(\"N1\",N1)"
for i in range(1,bacteria_num):
	bac_tuples = bac_tuples+",(\"N"+str(i+1)+"\",N"+str(i+1)+")"	
bac_tuples=bac_tuples+"]"


writer.write("\n"+bac_tuples)
writer.write("\nsorted_bacs= sorted(bac_tuples,key = lambda bac: bac[1])")
writer.write("\nhighest= sorted_bacs["+str(bacteria_num-1)+"][0][1:]")#the bacteria number that has the highest value
writer.write("\nlowest= sorted_bacs[0][0][1:]")#the bacteria number that has the lowest value
if bacteria_num > 2:
	writer.write("\nsecond_highest= sorted_bacs["+str(bacteria_num-2)+"][0][1:]")#the bacteria number that has the highest value

writer.write("\nN=["+bacterias+"] #forms a matrix of the substrate values at the end of that run")

writer.write("\n")


writer.write("\ndiagnostics_file = open(\"../../Data/"+location+"diagnostics-groups\"+str(functional_groups)+\".txt\",\"w\")")
writer.write("\ndiagnostics_file.write(\"This is "+name2+"\")")
writer.write("\ndiagnostics_file.write(\"\\nNumber of functional groups is: \"+str(functional_groups))")
writer.write("\ndiagnostics_file.write(\"\\nHighest bacteria is N \"+ highest)")
writer.write("\ndiagnostics_file.write(\"\\npopulation size: \"+str(N[int(highest)-1]))")
writer.write("\ndiagnostics_file.write(\"\\nWith enzyme kinetics:\")")
writer.write("\ndiagnostics_file.write(\"\\n\\nconversion:\"+str(conv[int(highest)-1]))")
writer.write("\ndiagnostics_file.write(\"\\nkcat: \"+str(kcat[int(highest)-1]))")
writer.write("\ndiagnostics_file.write(\"\\nkm: \"+str(km[int(highest)-1]))")
writer.write("\ndiagnostics_file.write(\"\\nE: \"+str(E[int(highest)-1]))")
writer.write("\ndiagnostics_file.write(\"\\ntoxicity: \"+str(tox[int(highest)-1]))")

if bacteria_num > 2:
	writer.write("\n")
	writer.write("\ndiagnostics_file.write(\"\\n\\nNext highest bacteria is N \"+ second_highest)")
	writer.write("\ndiagnostics_file.write(\"\\npopulation size: \"+str(N[int(second_highest)-1]))")
	writer.write("\ndiagnostics_file.write(\"\\nWith enzyme kinetics:\")")
	writer.write("\ndiagnostics_file.write(\"\\n\\nconversion:\"+str(conv[int(second_highest)-1]))")
	writer.write("\ndiagnostics_file.write(\"\\nkcat: \"+str(kcat[int(second_highest)-1]))")
	writer.write("\ndiagnostics_file.write(\"\\nkm: \"+str(km[int(second_highest)-1]))")
	writer.write("\ndiagnostics_file.write(\"\\nE: \"+str(E[int(second_highest)-1]))")
	writer.write("\ndiagnostics_file.write(\"\\ntoxicity: \"+str(tox[int(second_highest)-1]))")

writer.write("\ndiagnostics_file.write(\"\\n\\nwhole ending population is: \"+str(bac_tuples))")
writer.write("\ndiagnostics_file.write(\"\\nwhole ending enzyme allocation is: \\n\"+str(E))")


writer.write("\n\ndiagnostics_file.close()")


### CLOSE THE VARIOUS FILES IN THE SCRIPTS ###
	
writer.close()
# And the running script file itself is done being written #


#run the script that was just written, plot it in R and veiw the plots.
#~ subprocess.os.system("echo \"Running model\"")
subprocess.os.system("python "+ "../Scripts/"+name2+".py "+ str(random.seed(sys.argv[1])))
subprocess.os.system("echo \"Generating graphs... Please wait...\"")
subprocess.os.system("Rscript "+ "plot-outputs.R "+str(functional_groups)+" "+str(sys.argv[1])+ " "+ str(bacteria_num))
subprocess.os.system("echo \"Graphs saved\"")


#~ subprocess.os.system("last_file.sh")



