#!/usr/bin/env python
from __future__ import division

import csv
from pulp import *
import scipy
import numpy
from progressbar import ProgressBar
import random
random.seed(sys.argv[1])

timestep= 5 #how many timesteps should pass between sampling
global timelength
timelength= 10000 #how many timesteps in total to run the model for
dilution= 0.04
bacteria_num= 24
substrate_num=5
substrate_start=['1', '2', '3', '4', '5']
bacteria_start=1
functional_groups=2
bacteria_per_group=12

# # S E T  U P  C O D E # #
kcat= [[0.1 for x in range(substrate_num+1)]for x in range(bacteria_num)]
km= [[0.5 for x in range(substrate_num+1)]for x in range(bacteria_num)]
E= [[0.2 for x in range(substrate_num)]for x in range(bacteria_num)]
tox= [0.003217711158054604, 0.017169107690656004, 0.004394633945761939, 0.0008211538731596572, 0.014980858441925853, 0.01263450819703058, 0.002527492127616604, 0.0010137025430977126, 0.004177509142701945, 0.0012093244365359596, 0.010376420481607394, 0.015576741404024201, 0.0030306574214260668, 0.006083922184605799, 0.017496279258099098, 0.007088078833447094, 0.006380674763648184, 0.006187552885989654, 0.0048818868980900825, 0.008675581011183082, 0.003616592660533182, 0.004315447314684755, 0.015661018924689254, 0.0015707878472116972]
toxin_time = (3000, 5000)
antitoxin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]


#######################################
### GENERATES THE CONVERSION MATRIX ###
conv_groups = [[0.8, 0.75, 0.7000000000000001, 0.65, 0.6000000000000001] for x in range(bacteria_num)]
for i in range(0,bacteria_num):
	random_num= [random.uniform(-0.05,0.05) for x in range(bacteria_num)]
	for j in range(0,substrate_num):
		conv_groups[i][j]= conv_groups[i][j]+random_num[i]
		if (conv_groups[i][j]<0):
			conv_groups[i][j]=0
		if (conv_groups[i][j]>1):
			conv_groups[i][j]=1

conv=[[0 for x in range(substrate_num)]for x in range(bacteria_num)]
for h in range(0,int(bacteria_per_group)):
	for i in range(0,bacteria_num):
		for j in range(0,substrate_num):
			conv[i][j]=conv_groups[h][j]+random.uniform(-0.05,0.05)

#######################################

toxin_0=0
toxin_1=0.019
toxin= toxin_0

S1_0=1
S1=S1_0
S2_0=2
S2=S2_0
S3_0=3
S3=S3_0
S4_0=4
S4=S4_0
S5_0=5
S5=S5_0

###FUNCTIONS###


def bacteria_1(N1,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria1= N1+(conv[0][0]*(kcat[0][0]*E[0][0]*S1/(km[0][0]+S1))*N1)+(conv[0][1]*(kcat[0][1]*E[0][1]*S2/(km[0][1]+S2))*N1)+(conv[0][2]*(kcat[0][2]*E[0][2]*S3/(km[0][2]+S3))*N1)+(conv[0][3]*(kcat[0][3]*E[0][3]*S4/(km[0][3]+S4))*N1)+(conv[0][4]*(kcat[0][4]*E[0][4]*S5/(km[0][4]+S5))*N1)-(dilution*N1)-(N1*tox[0]*toxin)+(kcat[0][5]*antitoxin[0]*toxin/(km[0][5]+toxin))
	return (Bacteria1)

def bacteria_2(N2,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria2= N2+(conv[1][0]*(kcat[1][0]*E[1][0]*S1/(km[1][0]+S1))*N2)+(conv[1][1]*(kcat[1][1]*E[1][1]*S2/(km[1][1]+S2))*N2)+(conv[1][2]*(kcat[1][2]*E[1][2]*S3/(km[1][2]+S3))*N2)+(conv[1][3]*(kcat[1][3]*E[1][3]*S4/(km[1][3]+S4))*N2)+(conv[1][4]*(kcat[1][4]*E[1][4]*S5/(km[1][4]+S5))*N2)-(dilution*N2)-(N2*tox[1]*toxin)+(kcat[1][5]*antitoxin[1]*toxin/(km[1][5]+toxin))
	return (Bacteria2)

def bacteria_3(N3,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria3= N3+(conv[2][0]*(kcat[2][0]*E[2][0]*S1/(km[2][0]+S1))*N3)+(conv[2][1]*(kcat[2][1]*E[2][1]*S2/(km[2][1]+S2))*N3)+(conv[2][2]*(kcat[2][2]*E[2][2]*S3/(km[2][2]+S3))*N3)+(conv[2][3]*(kcat[2][3]*E[2][3]*S4/(km[2][3]+S4))*N3)+(conv[2][4]*(kcat[2][4]*E[2][4]*S5/(km[2][4]+S5))*N3)-(dilution*N3)-(N3*tox[2]*toxin)+(kcat[2][5]*antitoxin[2]*toxin/(km[2][5]+toxin))
	return (Bacteria3)

def bacteria_4(N4,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria4= N4+(conv[3][0]*(kcat[3][0]*E[3][0]*S1/(km[3][0]+S1))*N4)+(conv[3][1]*(kcat[3][1]*E[3][1]*S2/(km[3][1]+S2))*N4)+(conv[3][2]*(kcat[3][2]*E[3][2]*S3/(km[3][2]+S3))*N4)+(conv[3][3]*(kcat[3][3]*E[3][3]*S4/(km[3][3]+S4))*N4)+(conv[3][4]*(kcat[3][4]*E[3][4]*S5/(km[3][4]+S5))*N4)-(dilution*N4)-(N4*tox[3]*toxin)+(kcat[3][5]*antitoxin[3]*toxin/(km[3][5]+toxin))
	return (Bacteria4)

def bacteria_5(N5,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria5= N5+(conv[4][0]*(kcat[4][0]*E[4][0]*S1/(km[4][0]+S1))*N5)+(conv[4][1]*(kcat[4][1]*E[4][1]*S2/(km[4][1]+S2))*N5)+(conv[4][2]*(kcat[4][2]*E[4][2]*S3/(km[4][2]+S3))*N5)+(conv[4][3]*(kcat[4][3]*E[4][3]*S4/(km[4][3]+S4))*N5)+(conv[4][4]*(kcat[4][4]*E[4][4]*S5/(km[4][4]+S5))*N5)-(dilution*N5)-(N5*tox[4]*toxin)+(kcat[4][5]*antitoxin[4]*toxin/(km[4][5]+toxin))
	return (Bacteria5)

def bacteria_6(N6,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria6= N6+(conv[5][0]*(kcat[5][0]*E[5][0]*S1/(km[5][0]+S1))*N6)+(conv[5][1]*(kcat[5][1]*E[5][1]*S2/(km[5][1]+S2))*N6)+(conv[5][2]*(kcat[5][2]*E[5][2]*S3/(km[5][2]+S3))*N6)+(conv[5][3]*(kcat[5][3]*E[5][3]*S4/(km[5][3]+S4))*N6)+(conv[5][4]*(kcat[5][4]*E[5][4]*S5/(km[5][4]+S5))*N6)-(dilution*N6)-(N6*tox[5]*toxin)+(kcat[5][5]*antitoxin[5]*toxin/(km[5][5]+toxin))
	return (Bacteria6)

def bacteria_7(N7,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria7= N7+(conv[6][0]*(kcat[6][0]*E[6][0]*S1/(km[6][0]+S1))*N7)+(conv[6][1]*(kcat[6][1]*E[6][1]*S2/(km[6][1]+S2))*N7)+(conv[6][2]*(kcat[6][2]*E[6][2]*S3/(km[6][2]+S3))*N7)+(conv[6][3]*(kcat[6][3]*E[6][3]*S4/(km[6][3]+S4))*N7)+(conv[6][4]*(kcat[6][4]*E[6][4]*S5/(km[6][4]+S5))*N7)-(dilution*N7)-(N7*tox[6]*toxin)+(kcat[6][5]*antitoxin[6]*toxin/(km[6][5]+toxin))
	return (Bacteria7)

def bacteria_8(N8,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria8= N8+(conv[7][0]*(kcat[7][0]*E[7][0]*S1/(km[7][0]+S1))*N8)+(conv[7][1]*(kcat[7][1]*E[7][1]*S2/(km[7][1]+S2))*N8)+(conv[7][2]*(kcat[7][2]*E[7][2]*S3/(km[7][2]+S3))*N8)+(conv[7][3]*(kcat[7][3]*E[7][3]*S4/(km[7][3]+S4))*N8)+(conv[7][4]*(kcat[7][4]*E[7][4]*S5/(km[7][4]+S5))*N8)-(dilution*N8)-(N8*tox[7]*toxin)+(kcat[7][5]*antitoxin[7]*toxin/(km[7][5]+toxin))
	return (Bacteria8)

def bacteria_9(N9,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria9= N9+(conv[8][0]*(kcat[8][0]*E[8][0]*S1/(km[8][0]+S1))*N9)+(conv[8][1]*(kcat[8][1]*E[8][1]*S2/(km[8][1]+S2))*N9)+(conv[8][2]*(kcat[8][2]*E[8][2]*S3/(km[8][2]+S3))*N9)+(conv[8][3]*(kcat[8][3]*E[8][3]*S4/(km[8][3]+S4))*N9)+(conv[8][4]*(kcat[8][4]*E[8][4]*S5/(km[8][4]+S5))*N9)-(dilution*N9)-(N9*tox[8]*toxin)+(kcat[8][5]*antitoxin[8]*toxin/(km[8][5]+toxin))
	return (Bacteria9)

def bacteria_10(N10,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria10= N10+(conv[9][0]*(kcat[9][0]*E[9][0]*S1/(km[9][0]+S1))*N10)+(conv[9][1]*(kcat[9][1]*E[9][1]*S2/(km[9][1]+S2))*N10)+(conv[9][2]*(kcat[9][2]*E[9][2]*S3/(km[9][2]+S3))*N10)+(conv[9][3]*(kcat[9][3]*E[9][3]*S4/(km[9][3]+S4))*N10)+(conv[9][4]*(kcat[9][4]*E[9][4]*S5/(km[9][4]+S5))*N10)-(dilution*N10)-(N10*tox[9]*toxin)+(kcat[9][5]*antitoxin[9]*toxin/(km[9][5]+toxin))
	return (Bacteria10)

def bacteria_11(N11,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria11= N11+(conv[10][0]*(kcat[10][0]*E[10][0]*S1/(km[10][0]+S1))*N11)+(conv[10][1]*(kcat[10][1]*E[10][1]*S2/(km[10][1]+S2))*N11)+(conv[10][2]*(kcat[10][2]*E[10][2]*S3/(km[10][2]+S3))*N11)+(conv[10][3]*(kcat[10][3]*E[10][3]*S4/(km[10][3]+S4))*N11)+(conv[10][4]*(kcat[10][4]*E[10][4]*S5/(km[10][4]+S5))*N11)-(dilution*N11)-(N11*tox[10]*toxin)+(kcat[10][5]*antitoxin[10]*toxin/(km[10][5]+toxin))
	return (Bacteria11)

def bacteria_12(N12,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria12= N12+(conv[11][0]*(kcat[11][0]*E[11][0]*S1/(km[11][0]+S1))*N12)+(conv[11][1]*(kcat[11][1]*E[11][1]*S2/(km[11][1]+S2))*N12)+(conv[11][2]*(kcat[11][2]*E[11][2]*S3/(km[11][2]+S3))*N12)+(conv[11][3]*(kcat[11][3]*E[11][3]*S4/(km[11][3]+S4))*N12)+(conv[11][4]*(kcat[11][4]*E[11][4]*S5/(km[11][4]+S5))*N12)-(dilution*N12)-(N12*tox[11]*toxin)+(kcat[11][5]*antitoxin[11]*toxin/(km[11][5]+toxin))
	return (Bacteria12)

def bacteria_13(N13,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria13= N13+(conv[12][0]*(kcat[12][0]*E[12][0]*S1/(km[12][0]+S1))*N13)+(conv[12][1]*(kcat[12][1]*E[12][1]*S2/(km[12][1]+S2))*N13)+(conv[12][2]*(kcat[12][2]*E[12][2]*S3/(km[12][2]+S3))*N13)+(conv[12][3]*(kcat[12][3]*E[12][3]*S4/(km[12][3]+S4))*N13)+(conv[12][4]*(kcat[12][4]*E[12][4]*S5/(km[12][4]+S5))*N13)-(dilution*N13)-(N13*tox[12]*toxin)+(kcat[12][5]*antitoxin[12]*toxin/(km[12][5]+toxin))
	return (Bacteria13)

def bacteria_14(N14,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria14= N14+(conv[13][0]*(kcat[13][0]*E[13][0]*S1/(km[13][0]+S1))*N14)+(conv[13][1]*(kcat[13][1]*E[13][1]*S2/(km[13][1]+S2))*N14)+(conv[13][2]*(kcat[13][2]*E[13][2]*S3/(km[13][2]+S3))*N14)+(conv[13][3]*(kcat[13][3]*E[13][3]*S4/(km[13][3]+S4))*N14)+(conv[13][4]*(kcat[13][4]*E[13][4]*S5/(km[13][4]+S5))*N14)-(dilution*N14)-(N14*tox[13]*toxin)+(kcat[13][5]*antitoxin[13]*toxin/(km[13][5]+toxin))
	return (Bacteria14)

def bacteria_15(N15,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria15= N15+(conv[14][0]*(kcat[14][0]*E[14][0]*S1/(km[14][0]+S1))*N15)+(conv[14][1]*(kcat[14][1]*E[14][1]*S2/(km[14][1]+S2))*N15)+(conv[14][2]*(kcat[14][2]*E[14][2]*S3/(km[14][2]+S3))*N15)+(conv[14][3]*(kcat[14][3]*E[14][3]*S4/(km[14][3]+S4))*N15)+(conv[14][4]*(kcat[14][4]*E[14][4]*S5/(km[14][4]+S5))*N15)-(dilution*N15)-(N15*tox[14]*toxin)+(kcat[14][5]*antitoxin[14]*toxin/(km[14][5]+toxin))
	return (Bacteria15)

def bacteria_16(N16,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria16= N16+(conv[15][0]*(kcat[15][0]*E[15][0]*S1/(km[15][0]+S1))*N16)+(conv[15][1]*(kcat[15][1]*E[15][1]*S2/(km[15][1]+S2))*N16)+(conv[15][2]*(kcat[15][2]*E[15][2]*S3/(km[15][2]+S3))*N16)+(conv[15][3]*(kcat[15][3]*E[15][3]*S4/(km[15][3]+S4))*N16)+(conv[15][4]*(kcat[15][4]*E[15][4]*S5/(km[15][4]+S5))*N16)-(dilution*N16)-(N16*tox[15]*toxin)+(kcat[15][5]*antitoxin[15]*toxin/(km[15][5]+toxin))
	return (Bacteria16)

def bacteria_17(N17,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria17= N17+(conv[16][0]*(kcat[16][0]*E[16][0]*S1/(km[16][0]+S1))*N17)+(conv[16][1]*(kcat[16][1]*E[16][1]*S2/(km[16][1]+S2))*N17)+(conv[16][2]*(kcat[16][2]*E[16][2]*S3/(km[16][2]+S3))*N17)+(conv[16][3]*(kcat[16][3]*E[16][3]*S4/(km[16][3]+S4))*N17)+(conv[16][4]*(kcat[16][4]*E[16][4]*S5/(km[16][4]+S5))*N17)-(dilution*N17)-(N17*tox[16]*toxin)+(kcat[16][5]*antitoxin[16]*toxin/(km[16][5]+toxin))
	return (Bacteria17)

def bacteria_18(N18,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria18= N18+(conv[17][0]*(kcat[17][0]*E[17][0]*S1/(km[17][0]+S1))*N18)+(conv[17][1]*(kcat[17][1]*E[17][1]*S2/(km[17][1]+S2))*N18)+(conv[17][2]*(kcat[17][2]*E[17][2]*S3/(km[17][2]+S3))*N18)+(conv[17][3]*(kcat[17][3]*E[17][3]*S4/(km[17][3]+S4))*N18)+(conv[17][4]*(kcat[17][4]*E[17][4]*S5/(km[17][4]+S5))*N18)-(dilution*N18)-(N18*tox[17]*toxin)+(kcat[17][5]*antitoxin[17]*toxin/(km[17][5]+toxin))
	return (Bacteria18)

def bacteria_19(N19,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria19= N19+(conv[18][0]*(kcat[18][0]*E[18][0]*S1/(km[18][0]+S1))*N19)+(conv[18][1]*(kcat[18][1]*E[18][1]*S2/(km[18][1]+S2))*N19)+(conv[18][2]*(kcat[18][2]*E[18][2]*S3/(km[18][2]+S3))*N19)+(conv[18][3]*(kcat[18][3]*E[18][3]*S4/(km[18][3]+S4))*N19)+(conv[18][4]*(kcat[18][4]*E[18][4]*S5/(km[18][4]+S5))*N19)-(dilution*N19)-(N19*tox[18]*toxin)+(kcat[18][5]*antitoxin[18]*toxin/(km[18][5]+toxin))
	return (Bacteria19)

def bacteria_20(N20,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria20= N20+(conv[19][0]*(kcat[19][0]*E[19][0]*S1/(km[19][0]+S1))*N20)+(conv[19][1]*(kcat[19][1]*E[19][1]*S2/(km[19][1]+S2))*N20)+(conv[19][2]*(kcat[19][2]*E[19][2]*S3/(km[19][2]+S3))*N20)+(conv[19][3]*(kcat[19][3]*E[19][3]*S4/(km[19][3]+S4))*N20)+(conv[19][4]*(kcat[19][4]*E[19][4]*S5/(km[19][4]+S5))*N20)-(dilution*N20)-(N20*tox[19]*toxin)+(kcat[19][5]*antitoxin[19]*toxin/(km[19][5]+toxin))
	return (Bacteria20)

def bacteria_21(N21,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria21= N21+(conv[20][0]*(kcat[20][0]*E[20][0]*S1/(km[20][0]+S1))*N21)+(conv[20][1]*(kcat[20][1]*E[20][1]*S2/(km[20][1]+S2))*N21)+(conv[20][2]*(kcat[20][2]*E[20][2]*S3/(km[20][2]+S3))*N21)+(conv[20][3]*(kcat[20][3]*E[20][3]*S4/(km[20][3]+S4))*N21)+(conv[20][4]*(kcat[20][4]*E[20][4]*S5/(km[20][4]+S5))*N21)-(dilution*N21)-(N21*tox[20]*toxin)+(kcat[20][5]*antitoxin[20]*toxin/(km[20][5]+toxin))
	return (Bacteria21)

def bacteria_22(N22,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria22= N22+(conv[21][0]*(kcat[21][0]*E[21][0]*S1/(km[21][0]+S1))*N22)+(conv[21][1]*(kcat[21][1]*E[21][1]*S2/(km[21][1]+S2))*N22)+(conv[21][2]*(kcat[21][2]*E[21][2]*S3/(km[21][2]+S3))*N22)+(conv[21][3]*(kcat[21][3]*E[21][3]*S4/(km[21][3]+S4))*N22)+(conv[21][4]*(kcat[21][4]*E[21][4]*S5/(km[21][4]+S5))*N22)-(dilution*N22)-(N22*tox[21]*toxin)+(kcat[21][5]*antitoxin[21]*toxin/(km[21][5]+toxin))
	return (Bacteria22)

def bacteria_23(N23,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria23= N23+(conv[22][0]*(kcat[22][0]*E[22][0]*S1/(km[22][0]+S1))*N23)+(conv[22][1]*(kcat[22][1]*E[22][1]*S2/(km[22][1]+S2))*N23)+(conv[22][2]*(kcat[22][2]*E[22][2]*S3/(km[22][2]+S3))*N23)+(conv[22][3]*(kcat[22][3]*E[22][3]*S4/(km[22][3]+S4))*N23)+(conv[22][4]*(kcat[22][4]*E[22][4]*S5/(km[22][4]+S5))*N23)-(dilution*N23)-(N23*tox[22]*toxin)+(kcat[22][5]*antitoxin[22]*toxin/(km[22][5]+toxin))
	return (Bacteria23)

def bacteria_24(N24,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria24= N24+(conv[23][0]*(kcat[23][0]*E[23][0]*S1/(km[23][0]+S1))*N24)+(conv[23][1]*(kcat[23][1]*E[23][1]*S2/(km[23][1]+S2))*N24)+(conv[23][2]*(kcat[23][2]*E[23][2]*S3/(km[23][2]+S3))*N24)+(conv[23][3]*(kcat[23][3]*E[23][3]*S4/(km[23][3]+S4))*N24)+(conv[23][4]*(kcat[23][4]*E[23][4]*S5/(km[23][4]+S5))*N24)-(dilution*N24)-(N24*tox[23]*toxin)+(kcat[23][5]*antitoxin[23]*toxin/(km[23][5]+toxin))
	return (Bacteria24)

def substrate_1(S1,kcat,conv,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24):
	Substrate1=S1-((kcat[0][0]*E[0][0]*S1/(km[0][0]+S1))*N1)-((kcat[1][0]*E[1][0]*S1/(km[1][0]+S1))*N2)-((kcat[2][0]*E[2][0]*S1/(km[2][0]+S1))*N3)-((kcat[3][0]*E[3][0]*S1/(km[3][0]+S1))*N4)-((kcat[4][0]*E[4][0]*S1/(km[4][0]+S1))*N5)-((kcat[5][0]*E[5][0]*S1/(km[5][0]+S1))*N6)-((kcat[6][0]*E[6][0]*S1/(km[6][0]+S1))*N7)-((kcat[7][0]*E[7][0]*S1/(km[7][0]+S1))*N8)-((kcat[8][0]*E[8][0]*S1/(km[8][0]+S1))*N9)-((kcat[9][0]*E[9][0]*S1/(km[9][0]+S1))*N10)-((kcat[10][0]*E[10][0]*S1/(km[10][0]+S1))*N11)-((kcat[11][0]*E[11][0]*S1/(km[11][0]+S1))*N12)-((kcat[12][0]*E[12][0]*S1/(km[12][0]+S1))*N13)-((kcat[13][0]*E[13][0]*S1/(km[13][0]+S1))*N14)-((kcat[14][0]*E[14][0]*S1/(km[14][0]+S1))*N15)-((kcat[15][0]*E[15][0]*S1/(km[15][0]+S1))*N16)-((kcat[16][0]*E[16][0]*S1/(km[16][0]+S1))*N17)-((kcat[17][0]*E[17][0]*S1/(km[17][0]+S1))*N18)-((kcat[18][0]*E[18][0]*S1/(km[18][0]+S1))*N19)-((kcat[19][0]*E[19][0]*S1/(km[19][0]+S1))*N20)-((kcat[20][0]*E[20][0]*S1/(km[20][0]+S1))*N21)-((kcat[21][0]*E[21][0]*S1/(km[21][0]+S1))*N22)-((kcat[22][0]*E[22][0]*S1/(km[22][0]+S1))*N23)-((kcat[23][0]*E[23][0]*S1/(km[23][0]+S1))*N24)-dilution*S1+dilution*S1_0
	return (Substrate1)

def substrate_2(S2,kcat,conv,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24):
	Substrate2=S2-((kcat[0][1]*E[0][1]*S2/(km[0][1]+S2))*N1)-((kcat[1][1]*E[1][1]*S2/(km[1][1]+S2))*N2)-((kcat[2][1]*E[2][1]*S2/(km[2][1]+S2))*N3)-((kcat[3][1]*E[3][1]*S2/(km[3][1]+S2))*N4)-((kcat[4][1]*E[4][1]*S2/(km[4][1]+S2))*N5)-((kcat[5][1]*E[5][1]*S2/(km[5][1]+S2))*N6)-((kcat[6][1]*E[6][1]*S2/(km[6][1]+S2))*N7)-((kcat[7][1]*E[7][1]*S2/(km[7][1]+S2))*N8)-((kcat[8][1]*E[8][1]*S2/(km[8][1]+S2))*N9)-((kcat[9][1]*E[9][1]*S2/(km[9][1]+S2))*N10)-((kcat[10][1]*E[10][1]*S2/(km[10][1]+S2))*N11)-((kcat[11][1]*E[11][1]*S2/(km[11][1]+S2))*N12)-((kcat[12][1]*E[12][1]*S2/(km[12][1]+S2))*N13)-((kcat[13][1]*E[13][1]*S2/(km[13][1]+S2))*N14)-((kcat[14][1]*E[14][1]*S2/(km[14][1]+S2))*N15)-((kcat[15][1]*E[15][1]*S2/(km[15][1]+S2))*N16)-((kcat[16][1]*E[16][1]*S2/(km[16][1]+S2))*N17)-((kcat[17][1]*E[17][1]*S2/(km[17][1]+S2))*N18)-((kcat[18][1]*E[18][1]*S2/(km[18][1]+S2))*N19)-((kcat[19][1]*E[19][1]*S2/(km[19][1]+S2))*N20)-((kcat[20][1]*E[20][1]*S2/(km[20][1]+S2))*N21)-((kcat[21][1]*E[21][1]*S2/(km[21][1]+S2))*N22)-((kcat[22][1]*E[22][1]*S2/(km[22][1]+S2))*N23)-((kcat[23][1]*E[23][1]*S2/(km[23][1]+S2))*N24)-dilution*S2+dilution*S2_0
	return (Substrate2)

def substrate_3(S3,kcat,conv,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24):
	Substrate3=S3-((kcat[0][2]*E[0][2]*S3/(km[0][2]+S3))*N1)-((kcat[1][2]*E[1][2]*S3/(km[1][2]+S3))*N2)-((kcat[2][2]*E[2][2]*S3/(km[2][2]+S3))*N3)-((kcat[3][2]*E[3][2]*S3/(km[3][2]+S3))*N4)-((kcat[4][2]*E[4][2]*S3/(km[4][2]+S3))*N5)-((kcat[5][2]*E[5][2]*S3/(km[5][2]+S3))*N6)-((kcat[6][2]*E[6][2]*S3/(km[6][2]+S3))*N7)-((kcat[7][2]*E[7][2]*S3/(km[7][2]+S3))*N8)-((kcat[8][2]*E[8][2]*S3/(km[8][2]+S3))*N9)-((kcat[9][2]*E[9][2]*S3/(km[9][2]+S3))*N10)-((kcat[10][2]*E[10][2]*S3/(km[10][2]+S3))*N11)-((kcat[11][2]*E[11][2]*S3/(km[11][2]+S3))*N12)-((kcat[12][2]*E[12][2]*S3/(km[12][2]+S3))*N13)-((kcat[13][2]*E[13][2]*S3/(km[13][2]+S3))*N14)-((kcat[14][2]*E[14][2]*S3/(km[14][2]+S3))*N15)-((kcat[15][2]*E[15][2]*S3/(km[15][2]+S3))*N16)-((kcat[16][2]*E[16][2]*S3/(km[16][2]+S3))*N17)-((kcat[17][2]*E[17][2]*S3/(km[17][2]+S3))*N18)-((kcat[18][2]*E[18][2]*S3/(km[18][2]+S3))*N19)-((kcat[19][2]*E[19][2]*S3/(km[19][2]+S3))*N20)-((kcat[20][2]*E[20][2]*S3/(km[20][2]+S3))*N21)-((kcat[21][2]*E[21][2]*S3/(km[21][2]+S3))*N22)-((kcat[22][2]*E[22][2]*S3/(km[22][2]+S3))*N23)-((kcat[23][2]*E[23][2]*S3/(km[23][2]+S3))*N24)-dilution*S3+dilution*S3_0
	return (Substrate3)

def substrate_4(S4,kcat,conv,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24):
	Substrate4=S4-((kcat[0][3]*E[0][3]*S4/(km[0][3]+S4))*N1)-((kcat[1][3]*E[1][3]*S4/(km[1][3]+S4))*N2)-((kcat[2][3]*E[2][3]*S4/(km[2][3]+S4))*N3)-((kcat[3][3]*E[3][3]*S4/(km[3][3]+S4))*N4)-((kcat[4][3]*E[4][3]*S4/(km[4][3]+S4))*N5)-((kcat[5][3]*E[5][3]*S4/(km[5][3]+S4))*N6)-((kcat[6][3]*E[6][3]*S4/(km[6][3]+S4))*N7)-((kcat[7][3]*E[7][3]*S4/(km[7][3]+S4))*N8)-((kcat[8][3]*E[8][3]*S4/(km[8][3]+S4))*N9)-((kcat[9][3]*E[9][3]*S4/(km[9][3]+S4))*N10)-((kcat[10][3]*E[10][3]*S4/(km[10][3]+S4))*N11)-((kcat[11][3]*E[11][3]*S4/(km[11][3]+S4))*N12)-((kcat[12][3]*E[12][3]*S4/(km[12][3]+S4))*N13)-((kcat[13][3]*E[13][3]*S4/(km[13][3]+S4))*N14)-((kcat[14][3]*E[14][3]*S4/(km[14][3]+S4))*N15)-((kcat[15][3]*E[15][3]*S4/(km[15][3]+S4))*N16)-((kcat[16][3]*E[16][3]*S4/(km[16][3]+S4))*N17)-((kcat[17][3]*E[17][3]*S4/(km[17][3]+S4))*N18)-((kcat[18][3]*E[18][3]*S4/(km[18][3]+S4))*N19)-((kcat[19][3]*E[19][3]*S4/(km[19][3]+S4))*N20)-((kcat[20][3]*E[20][3]*S4/(km[20][3]+S4))*N21)-((kcat[21][3]*E[21][3]*S4/(km[21][3]+S4))*N22)-((kcat[22][3]*E[22][3]*S4/(km[22][3]+S4))*N23)-((kcat[23][3]*E[23][3]*S4/(km[23][3]+S4))*N24)-dilution*S4+dilution*S4_0
	return (Substrate4)

def substrate_5(S5,kcat,conv,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24):
	Substrate5=S5-((kcat[0][4]*E[0][4]*S5/(km[0][4]+S5))*N1)-((kcat[1][4]*E[1][4]*S5/(km[1][4]+S5))*N2)-((kcat[2][4]*E[2][4]*S5/(km[2][4]+S5))*N3)-((kcat[3][4]*E[3][4]*S5/(km[3][4]+S5))*N4)-((kcat[4][4]*E[4][4]*S5/(km[4][4]+S5))*N5)-((kcat[5][4]*E[5][4]*S5/(km[5][4]+S5))*N6)-((kcat[6][4]*E[6][4]*S5/(km[6][4]+S5))*N7)-((kcat[7][4]*E[7][4]*S5/(km[7][4]+S5))*N8)-((kcat[8][4]*E[8][4]*S5/(km[8][4]+S5))*N9)-((kcat[9][4]*E[9][4]*S5/(km[9][4]+S5))*N10)-((kcat[10][4]*E[10][4]*S5/(km[10][4]+S5))*N11)-((kcat[11][4]*E[11][4]*S5/(km[11][4]+S5))*N12)-((kcat[12][4]*E[12][4]*S5/(km[12][4]+S5))*N13)-((kcat[13][4]*E[13][4]*S5/(km[13][4]+S5))*N14)-((kcat[14][4]*E[14][4]*S5/(km[14][4]+S5))*N15)-((kcat[15][4]*E[15][4]*S5/(km[15][4]+S5))*N16)-((kcat[16][4]*E[16][4]*S5/(km[16][4]+S5))*N17)-((kcat[17][4]*E[17][4]*S5/(km[17][4]+S5))*N18)-((kcat[18][4]*E[18][4]*S5/(km[18][4]+S5))*N19)-((kcat[19][4]*E[19][4]*S5/(km[19][4]+S5))*N20)-((kcat[20][4]*E[20][4]*S5/(km[20][4]+S5))*N21)-((kcat[21][4]*E[21][4]*S5/(km[21][4]+S5))*N22)-((kcat[22][4]*E[22][4]*S5/(km[22][4]+S5))*N23)-((kcat[23][4]*E[23][4]*S5/(km[23][4]+S5))*N24)-dilution*S5+dilution*S5_0
	return (Substrate5)

def toxin_eqn(toxin,toxin_1):
	toxin = toxin + toxin_1 - (toxin*dilution)
	return(toxin)

#RUNNING CODE#
samplepoints= range(0,int(timelength)) #create a vector of the number
#of times you want to sample the population size
pbar= ProgressBar(samplepoints)#setting up the progessbar based on number of sample points
results_file = open('../../Data/med-model/Low/iter0model_output.csv', 'w')
writer = csv.writer(results_file, delimiter = ",")
enzyme_file = open('../../Data/med-model/Low/iter0enzyme_evolution.csv', 'w')
writer2 = csv.writer(enzyme_file, delimiter = ",")
writer.writerow(["Time","Bacteria_1","Bacteria_2","Bacteria_3","Bacteria_4","Bacteria_5","Bacteria_6","Bacteria_7","Bacteria_8","Bacteria_9","Bacteria_10","Bacteria_11","Bacteria_12","Bacteria_13","Bacteria_14","Bacteria_15","Bacteria_16","Bacteria_17","Bacteria_18","Bacteria_19","Bacteria_20","Bacteria_21","Bacteria_22","Bacteria_23","Bacteria_24", "Substrate_1", "Substrate_2", "Substrate_3", "Substrate_4", "Substrate_5", "Toxin"])
writer2.writerow(["Time","Bac1_Sub1","Bac1_Sub2","Bac1_Sub3","Bac1_Sub4","Bac1_Sub5","Bac2_Sub1","Bac2_Sub2","Bac2_Sub3","Bac2_Sub4","Bac2_Sub5","Bac3_Sub1","Bac3_Sub2","Bac3_Sub3","Bac3_Sub4","Bac3_Sub5","Bac4_Sub1","Bac4_Sub2","Bac4_Sub3","Bac4_Sub4","Bac4_Sub5","Bac5_Sub1","Bac5_Sub2","Bac5_Sub3","Bac5_Sub4","Bac5_Sub5","Bac6_Sub1","Bac6_Sub2","Bac6_Sub3","Bac6_Sub4","Bac6_Sub5","Bac7_Sub1","Bac7_Sub2","Bac7_Sub3","Bac7_Sub4","Bac7_Sub5","Bac8_Sub1","Bac8_Sub2","Bac8_Sub3","Bac8_Sub4","Bac8_Sub5","Bac9_Sub1","Bac9_Sub2","Bac9_Sub3","Bac9_Sub4","Bac9_Sub5","Bac10_Sub1","Bac10_Sub2","Bac10_Sub3","Bac10_Sub4","Bac10_Sub5","Bac11_Sub1","Bac11_Sub2","Bac11_Sub3","Bac11_Sub4","Bac11_Sub5","Bac12_Sub1","Bac12_Sub2","Bac12_Sub3","Bac12_Sub4","Bac12_Sub5","Bac13_Sub1","Bac13_Sub2","Bac13_Sub3","Bac13_Sub4","Bac13_Sub5","Bac14_Sub1","Bac14_Sub2","Bac14_Sub3","Bac14_Sub4","Bac14_Sub5","Bac15_Sub1","Bac15_Sub2","Bac15_Sub3","Bac15_Sub4","Bac15_Sub5","Bac16_Sub1","Bac16_Sub2","Bac16_Sub3","Bac16_Sub4","Bac16_Sub5","Bac17_Sub1","Bac17_Sub2","Bac17_Sub3","Bac17_Sub4","Bac17_Sub5","Bac18_Sub1","Bac18_Sub2","Bac18_Sub3","Bac18_Sub4","Bac18_Sub5","Bac19_Sub1","Bac19_Sub2","Bac19_Sub3","Bac19_Sub4","Bac19_Sub5","Bac20_Sub1","Bac20_Sub2","Bac20_Sub3","Bac20_Sub4","Bac20_Sub5","Bac21_Sub1","Bac21_Sub2","Bac21_Sub3","Bac21_Sub4","Bac21_Sub5","Bac22_Sub1","Bac22_Sub2","Bac22_Sub3","Bac22_Sub4","Bac22_Sub5","Bac23_Sub1","Bac23_Sub2","Bac23_Sub3","Bac23_Sub4","Bac23_Sub5","Bac24_Sub1","Bac24_Sub2","Bac24_Sub3","Bac24_Sub4","Bac24_Sub5"])
antitoxin_file = open('../../Data/med-model/Low/iter0antitoxin_evolution.csv','w')
writer3= csv.writer(antitoxin_file, delimiter = ",")
writer3.writerow(["Time","variable","value"])
N1=bacteria_start
N2=bacteria_start
N3=bacteria_start
N4=bacteria_start
N5=bacteria_start
N6=bacteria_start
N7=bacteria_start
N8=bacteria_start
N9=bacteria_start
N10=bacteria_start
N11=bacteria_start
N12=bacteria_start
N13=bacteria_start
N14=bacteria_start
N15=bacteria_start
N16=bacteria_start
N17=bacteria_start
N18=bacteria_start
N19=bacteria_start
N20=bacteria_start
N21=bacteria_start
N22=bacteria_start
N23=bacteria_start
N24=bacteria_start
writer.writerow([0,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,S1,S2,S3,S4,S5,toxin])


for i in pbar(samplepoints):
	#for each of the sample points call each bacterial equation once and write the output to the results file
	bacteria_1_output = bacteria_1(N1,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_1_output < 0.0001):
		N1=0
	elif (bacteria_1_output >10):
		N1=10
	else:
		N1 = bacteria_1_output

	bacteria_2_output = bacteria_2(N2,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_2_output < 0.0001):
		N2=0
	elif (bacteria_2_output >10):
		N2=10
	else:
		N2 = bacteria_2_output

	bacteria_3_output = bacteria_3(N3,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_3_output < 0.0001):
		N3=0
	elif (bacteria_3_output >10):
		N3=10
	else:
		N3 = bacteria_3_output

	bacteria_4_output = bacteria_4(N4,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_4_output < 0.0001):
		N4=0
	elif (bacteria_4_output >10):
		N4=10
	else:
		N4 = bacteria_4_output

	bacteria_5_output = bacteria_5(N5,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_5_output < 0.0001):
		N5=0
	elif (bacteria_5_output >10):
		N5=10
	else:
		N5 = bacteria_5_output

	bacteria_6_output = bacteria_6(N6,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_6_output < 0.0001):
		N6=0
	elif (bacteria_6_output >10):
		N6=10
	else:
		N6 = bacteria_6_output

	bacteria_7_output = bacteria_7(N7,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_7_output < 0.0001):
		N7=0
	elif (bacteria_7_output >10):
		N7=10
	else:
		N7 = bacteria_7_output

	bacteria_8_output = bacteria_8(N8,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_8_output < 0.0001):
		N8=0
	elif (bacteria_8_output >10):
		N8=10
	else:
		N8 = bacteria_8_output

	bacteria_9_output = bacteria_9(N9,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_9_output < 0.0001):
		N9=0
	elif (bacteria_9_output >10):
		N9=10
	else:
		N9 = bacteria_9_output

	bacteria_10_output = bacteria_10(N10,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_10_output < 0.0001):
		N10=0
	elif (bacteria_10_output >10):
		N10=10
	else:
		N10 = bacteria_10_output

	bacteria_11_output = bacteria_11(N11,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_11_output < 0.0001):
		N11=0
	elif (bacteria_11_output >10):
		N11=10
	else:
		N11 = bacteria_11_output

	bacteria_12_output = bacteria_12(N12,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_12_output < 0.0001):
		N12=0
	elif (bacteria_12_output >10):
		N12=10
	else:
		N12 = bacteria_12_output

	bacteria_13_output = bacteria_13(N13,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_13_output < 0.0001):
		N13=0
	elif (bacteria_13_output >10):
		N13=10
	else:
		N13 = bacteria_13_output

	bacteria_14_output = bacteria_14(N14,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_14_output < 0.0001):
		N14=0
	elif (bacteria_14_output >10):
		N14=10
	else:
		N14 = bacteria_14_output

	bacteria_15_output = bacteria_15(N15,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_15_output < 0.0001):
		N15=0
	elif (bacteria_15_output >10):
		N15=10
	else:
		N15 = bacteria_15_output

	bacteria_16_output = bacteria_16(N16,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_16_output < 0.0001):
		N16=0
	elif (bacteria_16_output >10):
		N16=10
	else:
		N16 = bacteria_16_output

	bacteria_17_output = bacteria_17(N17,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_17_output < 0.0001):
		N17=0
	elif (bacteria_17_output >10):
		N17=10
	else:
		N17 = bacteria_17_output

	bacteria_18_output = bacteria_18(N18,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_18_output < 0.0001):
		N18=0
	elif (bacteria_18_output >10):
		N18=10
	else:
		N18 = bacteria_18_output

	bacteria_19_output = bacteria_19(N19,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_19_output < 0.0001):
		N19=0
	elif (bacteria_19_output >10):
		N19=10
	else:
		N19 = bacteria_19_output

	bacteria_20_output = bacteria_20(N20,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_20_output < 0.0001):
		N20=0
	elif (bacteria_20_output >10):
		N20=10
	else:
		N20 = bacteria_20_output

	bacteria_21_output = bacteria_21(N21,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_21_output < 0.0001):
		N21=0
	elif (bacteria_21_output >10):
		N21=10
	else:
		N21 = bacteria_21_output

	bacteria_22_output = bacteria_22(N22,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_22_output < 0.0001):
		N22=0
	elif (bacteria_22_output >10):
		N22=10
	else:
		N22 = bacteria_22_output

	bacteria_23_output = bacteria_23(N23,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_23_output < 0.0001):
		N23=0
	elif (bacteria_23_output >10):
		N23=10
	else:
		N23 = bacteria_23_output

	bacteria_24_output = bacteria_24(N24,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_24_output < 0.0001):
		N24=0
	elif (bacteria_24_output >10):
		N24=10
	else:
		N24 = bacteria_24_output

	new_substrate_1= substrate_1(S1,kcat,km,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24)
	if (new_substrate_1 < 0.0001):
		S1=0
	else:
		S1=new_substrate_1


	new_substrate_2= substrate_2(S2,kcat,km,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24)
	if (new_substrate_2 < 0.0001):
		S2=0
	else:
		S2=new_substrate_2


	new_substrate_3= substrate_3(S3,kcat,km,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24)
	if (new_substrate_3 < 0.0001):
		S3=0
	else:
		S3=new_substrate_3


	new_substrate_4= substrate_4(S4,kcat,km,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24)
	if (new_substrate_4 < 0.0001):
		S4=0
	else:
		S4=new_substrate_4


	new_substrate_5= substrate_5(S5,kcat,km,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24)
	if (new_substrate_5 < 0.0001):
		S5=0
	else:
		S5=new_substrate_5


	if i in range(3000, 5000):
		new_toxin=toxin_eqn(toxin,toxin_1)
		toxin= new_toxin


	else:
		new_toxin=toxin_eqn(toxin,toxin_0)
		toxin= new_toxin

	S=[S1,S2,S3,S4,S5] #forms a matrix of the substrate values at the end of that run

### THE ENZYME EVOLUTION CODE ###

	for bacteria_ID in range(bacteria_num):
		if (i%5 == 0):
			N=[N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24] #forms a matrix of the substrate values at the end of that run
			if N[bacteria_ID] > 0:
				mu = [random.uniform(0, 0.02) for x in range(substrate_num)]
				mut = random.uniform(0, 0.02)
				a=LpVariable('a',0,1)
				b=LpVariable('b',0,1)
				c=LpVariable('c',0,1)
				d=LpVariable('d',0,1)
				e=LpVariable('e',0,1)
				anti = LpVariable('anti',0,1)
				prob = LpProblem("myProblem",LpMaximize)
				prob += a+b+c+d+e+anti<=1 #the constraint that they must all sum to 1
				prob+= (conv[bacteria_ID][0]*(kcat[bacteria_ID][0]*a*S1/(km[bacteria_ID][0]+S1)))+(conv[bacteria_ID][1]*(kcat[bacteria_ID][1]*b*S2/(km[bacteria_ID][1]+S2)))+(conv[bacteria_ID][2]*(kcat[bacteria_ID][2]*c*S3/(km[bacteria_ID][2]+S3)))+(conv[bacteria_ID][3]*(kcat[bacteria_ID][3]*d*S4/(km[bacteria_ID][3]+S4)))+(conv[bacteria_ID][4]*(kcat[bacteria_ID][4]*e*S5/(km[bacteria_ID][4]+S5)))+(kcat[bacteria_ID][5]*anti*toxin/(km[bacteria_ID][5]+toxin))

				status = prob.solve()
				LpStatus[status]

				best_enzyme= [int(value(a)),int(value(b)),int(value(c)),int(value(d)),int(value(e))]
				E_diff = numpy.array(E[bacteria_ID])-numpy.array(best_enzyme)
				E_diff_matrix = E_diff*mu
				E[bacteria_ID] = E[bacteria_ID] - E_diff_matrix

				best_antitoxin=int(value(anti))
				antitoxin_diff = best_antitoxin-antitoxin[bacteria_ID]
				new_antitoxin = antitoxin_diff*mut
				antitoxin[bacteria_ID] = antitoxin[bacteria_ID]+ new_antitoxin
				writer3.writerow([i+1,"Bac"+str(bacteria_ID+1),antitoxin[bacteria_ID]])

			elif N[bacteria_ID] == 0 :
				E[bacteria_ID] = E[bacteria_ID] * 0
				antitoxin[bacteria_ID] = antitoxin[bacteria_ID] * 0

	if i % timestep == 0:
		writer2.writerow([i+1,E[0][0],E[0][1],E[0][2],E[0][3],E[0][4],E[1][0],E[1][1],E[1][2],E[1][3],E[1][4],E[2][0],E[2][1],E[2][2],E[2][3],E[2][4],E[3][0],E[3][1],E[3][2],E[3][3],E[3][4],E[4][0],E[4][1],E[4][2],E[4][3],E[4][4],E[5][0],E[5][1],E[5][2],E[5][3],E[5][4],E[6][0],E[6][1],E[6][2],E[6][3],E[6][4],E[7][0],E[7][1],E[7][2],E[7][3],E[7][4],E[8][0],E[8][1],E[8][2],E[8][3],E[8][4],E[9][0],E[9][1],E[9][2],E[9][3],E[9][4],E[10][0],E[10][1],E[10][2],E[10][3],E[10][4],E[11][0],E[11][1],E[11][2],E[11][3],E[11][4],E[12][0],E[12][1],E[12][2],E[12][3],E[12][4],E[13][0],E[13][1],E[13][2],E[13][3],E[13][4],E[14][0],E[14][1],E[14][2],E[14][3],E[14][4],E[15][0],E[15][1],E[15][2],E[15][3],E[15][4],E[16][0],E[16][1],E[16][2],E[16][3],E[16][4],E[17][0],E[17][1],E[17][2],E[17][3],E[17][4],E[18][0],E[18][1],E[18][2],E[18][3],E[18][4],E[19][0],E[19][1],E[19][2],E[19][3],E[19][4],E[20][0],E[20][1],E[20][2],E[20][3],E[20][4],E[21][0],E[21][1],E[21][2],E[21][3],E[21][4],E[22][0],E[22][1],E[22][2],E[22][3],E[22][4],E[23][0],E[23][1],E[23][2],E[23][3],E[23][4]])
		writer.writerow([i+1,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,S1,S2,S3,S4,S5,toxin])

print ("Model run successfully")

results_file.close()
enzyme_file.close()
antitoxin_file.close()


bac_tuples = [("N1",N1),("N2",N2),("N3",N3),("N4",N4),("N5",N5),("N6",N6),("N7",N7),("N8",N8),("N9",N9),("N10",N10),("N11",N11),("N12",N12),("N13",N13),("N14",N14),("N15",N15),("N16",N16),("N17",N17),("N18",N18),("N19",N19),("N20",N20),("N21",N21),("N22",N22),("N23",N23),("N24",N24)]
sorted_bacs= sorted(bac_tuples,key = lambda bac: bac[1])
highest= sorted_bacs[23][0][1:]
lowest= sorted_bacs[0][0][1:]
second_highest= sorted_bacs[22][0][1:]
N=[N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24] #forms a matrix of the substrate values at the end of that run

diagnostics_file = open("../../Data/med-model/Low/iter0diagnostics-groups"+str(functional_groups)+".txt","w")
diagnostics_file.write("This is med-low-iter0")
diagnostics_file.write("\nNumber of functional groups is: "+str(functional_groups))
diagnostics_file.write("\nHighest bacteria is N "+ highest)
diagnostics_file.write("\npopulation size: "+str(N[int(highest)-1]))
diagnostics_file.write("\nWith enzyme kinetics:")
diagnostics_file.write("\n\nconversion:"+str(conv[int(highest)-1]))
diagnostics_file.write("\nkcat: "+str(kcat[int(highest)-1]))
diagnostics_file.write("\nkm: "+str(km[int(highest)-1]))
diagnostics_file.write("\nE: "+str(E[int(highest)-1]))
diagnostics_file.write("\ntoxicity: "+str(tox[int(highest)-1]))

diagnostics_file.write("\n\nNext highest bacteria is N "+ second_highest)
diagnostics_file.write("\npopulation size: "+str(N[int(second_highest)-1]))
diagnostics_file.write("\nWith enzyme kinetics:")
diagnostics_file.write("\n\nconversion:"+str(conv[int(second_highest)-1]))
diagnostics_file.write("\nkcat: "+str(kcat[int(second_highest)-1]))
diagnostics_file.write("\nkm: "+str(km[int(second_highest)-1]))
diagnostics_file.write("\nE: "+str(E[int(second_highest)-1]))
diagnostics_file.write("\ntoxicity: "+str(tox[int(second_highest)-1]))
diagnostics_file.write("\n\nwhole ending population is: "+str(bac_tuples))
diagnostics_file.write("\nwhole ending enzyme allocation is: \n"+str(E))

diagnostics_file.close()