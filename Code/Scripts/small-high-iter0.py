#!/usr/bin/env python
from __future__ import division

import csv
from pulp import *
import scipy
import numpy
from progressbar import ProgressBar
import random
random.seed(sys.argv[1])

timestep= 5 #how many timesteps should pass between sampling
global timelength
timelength= 10000 #how many timesteps in total to run the model for
dilution= 0.04
bacteria_num= 5
substrate_num=5
substrate_start=['1', '2', '3', '4', '5']
bacteria_start=1
functional_groups=5
bacteria_per_group=1

# # S E T  U P  C O D E # #
kcat= [[0.1 for x in range(substrate_num+1)]for x in range(bacteria_num)]
km= [[0.5 for x in range(substrate_num+1)]for x in range(bacteria_num)]
E= [[0.2 for x in range(substrate_num)]for x in range(bacteria_num)]
tox= [0.003217711158054604, 0.017169107690656004, 0.004394633945761939, 0.0008211538731596572, 0.014980858441925853]
toxin_time = (3000, 5000)
antitoxin = [0, 0, 0, 0, 0]


#######################################
### GENERATES THE CONVERSION MATRIX ###
conv_groups = [[0.8, 0.75, 0.7000000000000001, 0.65, 0.6000000000000001] for x in range(bacteria_num)]
for i in range(0,bacteria_num):
	random_num= [random.uniform(-0.05,0.05) for x in range(bacteria_num)]
	for j in range(0,substrate_num):
		conv_groups[i][j]= conv_groups[i][j]+random_num[i]
		if (conv_groups[i][j]<0):
			conv_groups[i][j]=0
		if (conv_groups[i][j]>1):
			conv_groups[i][j]=1

conv=[[0 for x in range(substrate_num)]for x in range(bacteria_num)]
for h in range(0,int(bacteria_per_group)):
	for i in range(0,bacteria_num):
		for j in range(0,substrate_num):
			conv[i][j]=conv_groups[h][j]+random.uniform(-0.05,0.05)

#######################################

toxin_0=0
toxin_1=0.019
toxin= toxin_0

S1_0=1
S1=S1_0
S2_0=2
S2=S2_0
S3_0=3
S3=S3_0
S4_0=4
S4=S4_0
S5_0=5
S5=S5_0

###FUNCTIONS###


def bacteria_1(N1,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria1= N1+(conv[0][0]*(kcat[0][0]*E[0][0]*S1/(km[0][0]+S1))*N1)+(conv[0][1]*(kcat[0][1]*E[0][1]*S2/(km[0][1]+S2))*N1)+(conv[0][2]*(kcat[0][2]*E[0][2]*S3/(km[0][2]+S3))*N1)+(conv[0][3]*(kcat[0][3]*E[0][3]*S4/(km[0][3]+S4))*N1)+(conv[0][4]*(kcat[0][4]*E[0][4]*S5/(km[0][4]+S5))*N1)-(dilution*N1)-(N1*tox[0]*toxin)+(kcat[0][5]*antitoxin[0]*toxin/(km[0][5]+toxin))
	return (Bacteria1)

def bacteria_2(N2,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria2= N2+(conv[1][0]*(kcat[1][0]*E[1][0]*S1/(km[1][0]+S1))*N2)+(conv[1][1]*(kcat[1][1]*E[1][1]*S2/(km[1][1]+S2))*N2)+(conv[1][2]*(kcat[1][2]*E[1][2]*S3/(km[1][2]+S3))*N2)+(conv[1][3]*(kcat[1][3]*E[1][3]*S4/(km[1][3]+S4))*N2)+(conv[1][4]*(kcat[1][4]*E[1][4]*S5/(km[1][4]+S5))*N2)-(dilution*N2)-(N2*tox[1]*toxin)+(kcat[1][5]*antitoxin[1]*toxin/(km[1][5]+toxin))
	return (Bacteria2)

def bacteria_3(N3,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria3= N3+(conv[2][0]*(kcat[2][0]*E[2][0]*S1/(km[2][0]+S1))*N3)+(conv[2][1]*(kcat[2][1]*E[2][1]*S2/(km[2][1]+S2))*N3)+(conv[2][2]*(kcat[2][2]*E[2][2]*S3/(km[2][2]+S3))*N3)+(conv[2][3]*(kcat[2][3]*E[2][3]*S4/(km[2][3]+S4))*N3)+(conv[2][4]*(kcat[2][4]*E[2][4]*S5/(km[2][4]+S5))*N3)-(dilution*N3)-(N3*tox[2]*toxin)+(kcat[2][5]*antitoxin[2]*toxin/(km[2][5]+toxin))
	return (Bacteria3)

def bacteria_4(N4,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria4= N4+(conv[3][0]*(kcat[3][0]*E[3][0]*S1/(km[3][0]+S1))*N4)+(conv[3][1]*(kcat[3][1]*E[3][1]*S2/(km[3][1]+S2))*N4)+(conv[3][2]*(kcat[3][2]*E[3][2]*S3/(km[3][2]+S3))*N4)+(conv[3][3]*(kcat[3][3]*E[3][3]*S4/(km[3][3]+S4))*N4)+(conv[3][4]*(kcat[3][4]*E[3][4]*S5/(km[3][4]+S5))*N4)-(dilution*N4)-(N4*tox[3]*toxin)+(kcat[3][5]*antitoxin[3]*toxin/(km[3][5]+toxin))
	return (Bacteria4)

def bacteria_5(N5,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria5= N5+(conv[4][0]*(kcat[4][0]*E[4][0]*S1/(km[4][0]+S1))*N5)+(conv[4][1]*(kcat[4][1]*E[4][1]*S2/(km[4][1]+S2))*N5)+(conv[4][2]*(kcat[4][2]*E[4][2]*S3/(km[4][2]+S3))*N5)+(conv[4][3]*(kcat[4][3]*E[4][3]*S4/(km[4][3]+S4))*N5)+(conv[4][4]*(kcat[4][4]*E[4][4]*S5/(km[4][4]+S5))*N5)-(dilution*N5)-(N5*tox[4]*toxin)+(kcat[4][5]*antitoxin[4]*toxin/(km[4][5]+toxin))
	return (Bacteria5)

def substrate_1(S1,kcat,conv,E,dilution,N1,N2,N3,N4,N5):
	Substrate1=S1-((kcat[0][0]*E[0][0]*S1/(km[0][0]+S1))*N1)-((kcat[1][0]*E[1][0]*S1/(km[1][0]+S1))*N2)-((kcat[2][0]*E[2][0]*S1/(km[2][0]+S1))*N3)-((kcat[3][0]*E[3][0]*S1/(km[3][0]+S1))*N4)-((kcat[4][0]*E[4][0]*S1/(km[4][0]+S1))*N5)-dilution*S1+dilution*S1_0
	return (Substrate1)

def substrate_2(S2,kcat,conv,E,dilution,N1,N2,N3,N4,N5):
	Substrate2=S2-((kcat[0][1]*E[0][1]*S2/(km[0][1]+S2))*N1)-((kcat[1][1]*E[1][1]*S2/(km[1][1]+S2))*N2)-((kcat[2][1]*E[2][1]*S2/(km[2][1]+S2))*N3)-((kcat[3][1]*E[3][1]*S2/(km[3][1]+S2))*N4)-((kcat[4][1]*E[4][1]*S2/(km[4][1]+S2))*N5)-dilution*S2+dilution*S2_0
	return (Substrate2)

def substrate_3(S3,kcat,conv,E,dilution,N1,N2,N3,N4,N5):
	Substrate3=S3-((kcat[0][2]*E[0][2]*S3/(km[0][2]+S3))*N1)-((kcat[1][2]*E[1][2]*S3/(km[1][2]+S3))*N2)-((kcat[2][2]*E[2][2]*S3/(km[2][2]+S3))*N3)-((kcat[3][2]*E[3][2]*S3/(km[3][2]+S3))*N4)-((kcat[4][2]*E[4][2]*S3/(km[4][2]+S3))*N5)-dilution*S3+dilution*S3_0
	return (Substrate3)

def substrate_4(S4,kcat,conv,E,dilution,N1,N2,N3,N4,N5):
	Substrate4=S4-((kcat[0][3]*E[0][3]*S4/(km[0][3]+S4))*N1)-((kcat[1][3]*E[1][3]*S4/(km[1][3]+S4))*N2)-((kcat[2][3]*E[2][3]*S4/(km[2][3]+S4))*N3)-((kcat[3][3]*E[3][3]*S4/(km[3][3]+S4))*N4)-((kcat[4][3]*E[4][3]*S4/(km[4][3]+S4))*N5)-dilution*S4+dilution*S4_0
	return (Substrate4)

def substrate_5(S5,kcat,conv,E,dilution,N1,N2,N3,N4,N5):
	Substrate5=S5-((kcat[0][4]*E[0][4]*S5/(km[0][4]+S5))*N1)-((kcat[1][4]*E[1][4]*S5/(km[1][4]+S5))*N2)-((kcat[2][4]*E[2][4]*S5/(km[2][4]+S5))*N3)-((kcat[3][4]*E[3][4]*S5/(km[3][4]+S5))*N4)-((kcat[4][4]*E[4][4]*S5/(km[4][4]+S5))*N5)-dilution*S5+dilution*S5_0
	return (Substrate5)

def toxin_eqn(toxin,toxin_1):
	toxin = toxin + toxin_1 - (toxin*dilution)
	return(toxin)

#RUNNING CODE#
samplepoints= range(0,int(timelength)) #create a vector of the number
#of times you want to sample the population size
pbar= ProgressBar(samplepoints)#setting up the progessbar based on number of sample points
results_file = open('../../Data/small-model/High/iter0model_output.csv', 'w')
writer = csv.writer(results_file, delimiter = ",")
enzyme_file = open('../../Data/small-model/High/iter0enzyme_evolution.csv', 'w')
writer2 = csv.writer(enzyme_file, delimiter = ",")
writer.writerow(["Time","Bacteria_1","Bacteria_2","Bacteria_3","Bacteria_4","Bacteria_5", "Substrate_1", "Substrate_2", "Substrate_3", "Substrate_4", "Substrate_5", "Toxin"])
writer2.writerow(["Time","Bac1_Sub1","Bac1_Sub2","Bac1_Sub3","Bac1_Sub4","Bac1_Sub5","Bac2_Sub1","Bac2_Sub2","Bac2_Sub3","Bac2_Sub4","Bac2_Sub5","Bac3_Sub1","Bac3_Sub2","Bac3_Sub3","Bac3_Sub4","Bac3_Sub5","Bac4_Sub1","Bac4_Sub2","Bac4_Sub3","Bac4_Sub4","Bac4_Sub5","Bac5_Sub1","Bac5_Sub2","Bac5_Sub3","Bac5_Sub4","Bac5_Sub5"])
antitoxin_file = open('../../Data/small-model/High/iter0antitoxin_evolution.csv','w')
writer3= csv.writer(antitoxin_file, delimiter = ",")
writer3.writerow(["Time","variable","value"])
N1=bacteria_start
N2=bacteria_start
N3=bacteria_start
N4=bacteria_start
N5=bacteria_start
writer.writerow([0,N1,N2,N3,N4,N5,S1,S2,S3,S4,S5,toxin])


for i in pbar(samplepoints):
	#for each of the sample points call each bacterial equation once and write the output to the results file
	bacteria_1_output = bacteria_1(N1,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_1_output < 0.0001):
		N1=0
	elif (bacteria_1_output >10):
		N1=10
	else:
		N1 = bacteria_1_output

	bacteria_2_output = bacteria_2(N2,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_2_output < 0.0001):
		N2=0
	elif (bacteria_2_output >10):
		N2=10
	else:
		N2 = bacteria_2_output

	bacteria_3_output = bacteria_3(N3,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_3_output < 0.0001):
		N3=0
	elif (bacteria_3_output >10):
		N3=10
	else:
		N3 = bacteria_3_output

	bacteria_4_output = bacteria_4(N4,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_4_output < 0.0001):
		N4=0
	elif (bacteria_4_output >10):
		N4=10
	else:
		N4 = bacteria_4_output

	bacteria_5_output = bacteria_5(N5,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_5_output < 0.0001):
		N5=0
	elif (bacteria_5_output >10):
		N5=10
	else:
		N5 = bacteria_5_output

	new_substrate_1= substrate_1(S1,kcat,km,E,dilution,N1,N2,N3,N4,N5)
	if (new_substrate_1 < 0.0001):
		S1=0
	else:
		S1=new_substrate_1


	new_substrate_2= substrate_2(S2,kcat,km,E,dilution,N1,N2,N3,N4,N5)
	if (new_substrate_2 < 0.0001):
		S2=0
	else:
		S2=new_substrate_2


	new_substrate_3= substrate_3(S3,kcat,km,E,dilution,N1,N2,N3,N4,N5)
	if (new_substrate_3 < 0.0001):
		S3=0
	else:
		S3=new_substrate_3


	new_substrate_4= substrate_4(S4,kcat,km,E,dilution,N1,N2,N3,N4,N5)
	if (new_substrate_4 < 0.0001):
		S4=0
	else:
		S4=new_substrate_4


	new_substrate_5= substrate_5(S5,kcat,km,E,dilution,N1,N2,N3,N4,N5)
	if (new_substrate_5 < 0.0001):
		S5=0
	else:
		S5=new_substrate_5


	if i in range(3000, 5000):
		new_toxin=toxin_eqn(toxin,toxin_1)
		toxin= new_toxin


	else:
		new_toxin=toxin_eqn(toxin,toxin_0)
		toxin= new_toxin

	S=[S1,S2,S3,S4,S5] #forms a matrix of the substrate values at the end of that run

### THE ENZYME EVOLUTION CODE ###

	for bacteria_ID in range(bacteria_num):
		if (i%5 == 0):
			N=[N1,N2,N3,N4,N5] #forms a matrix of the substrate values at the end of that run
			if N[bacteria_ID] > 0:
				mu = [random.uniform(0, 0.02) for x in range(substrate_num)]
				mut = random.uniform(0, 0.02)
				a=LpVariable('a',0,1)
				b=LpVariable('b',0,1)
				c=LpVariable('c',0,1)
				d=LpVariable('d',0,1)
				e=LpVariable('e',0,1)
				anti = LpVariable('anti',0,1)
				prob = LpProblem("myProblem",LpMaximize)
				prob += a+b+c+d+e+anti<=1 #the constraint that they must all sum to 1
				prob+= (conv[bacteria_ID][0]*(kcat[bacteria_ID][0]*a*S1/(km[bacteria_ID][0]+S1)))+(conv[bacteria_ID][1]*(kcat[bacteria_ID][1]*b*S2/(km[bacteria_ID][1]+S2)))+(conv[bacteria_ID][2]*(kcat[bacteria_ID][2]*c*S3/(km[bacteria_ID][2]+S3)))+(conv[bacteria_ID][3]*(kcat[bacteria_ID][3]*d*S4/(km[bacteria_ID][3]+S4)))+(conv[bacteria_ID][4]*(kcat[bacteria_ID][4]*e*S5/(km[bacteria_ID][4]+S5)))+(kcat[bacteria_ID][5]*anti*toxin/(km[bacteria_ID][5]+toxin))

				status = prob.solve()
				LpStatus[status]

				best_enzyme= [int(value(a)),int(value(b)),int(value(c)),int(value(d)),int(value(e))]
				E_diff = numpy.array(E[bacteria_ID])-numpy.array(best_enzyme)
				E_diff_matrix = E_diff*mu
				E[bacteria_ID] = E[bacteria_ID] - E_diff_matrix

				best_antitoxin=int(value(anti))
				antitoxin_diff = best_antitoxin-antitoxin[bacteria_ID]
				new_antitoxin = antitoxin_diff*mut
				antitoxin[bacteria_ID] = antitoxin[bacteria_ID]+ new_antitoxin
				writer3.writerow([i+1,"Bac"+str(bacteria_ID+1),antitoxin[bacteria_ID]])

			elif N[bacteria_ID] == 0 :
				E[bacteria_ID] = E[bacteria_ID] * 0
				antitoxin[bacteria_ID] = antitoxin[bacteria_ID] * 0

	if i % timestep == 0:
		writer2.writerow([i+1,E[0][0],E[0][1],E[0][2],E[0][3],E[0][4],E[1][0],E[1][1],E[1][2],E[1][3],E[1][4],E[2][0],E[2][1],E[2][2],E[2][3],E[2][4],E[3][0],E[3][1],E[3][2],E[3][3],E[3][4],E[4][0],E[4][1],E[4][2],E[4][3],E[4][4]])
		writer.writerow([i+1,N1,N2,N3,N4,N5,S1,S2,S3,S4,S5,toxin])

print ("Model run successfully")

results_file.close()
enzyme_file.close()
antitoxin_file.close()


bac_tuples = [("N1",N1),("N2",N2),("N3",N3),("N4",N4),("N5",N5)]
sorted_bacs= sorted(bac_tuples,key = lambda bac: bac[1])
highest= sorted_bacs[4][0][1:]
lowest= sorted_bacs[0][0][1:]
second_highest= sorted_bacs[3][0][1:]
N=[N1,N2,N3,N4,N5] #forms a matrix of the substrate values at the end of that run

diagnostics_file = open("../../Data/small-model/High/iter0diagnostics-groups"+str(functional_groups)+".txt","w")
diagnostics_file.write("This is small-high-iter0")
diagnostics_file.write("\nNumber of functional groups is: "+str(functional_groups))
diagnostics_file.write("\nHighest bacteria is N "+ highest)
diagnostics_file.write("\npopulation size: "+str(N[int(highest)-1]))
diagnostics_file.write("\nWith enzyme kinetics:")
diagnostics_file.write("\n\nconversion:"+str(conv[int(highest)-1]))
diagnostics_file.write("\nkcat: "+str(kcat[int(highest)-1]))
diagnostics_file.write("\nkm: "+str(km[int(highest)-1]))
diagnostics_file.write("\nE: "+str(E[int(highest)-1]))
diagnostics_file.write("\ntoxicity: "+str(tox[int(highest)-1]))

diagnostics_file.write("\n\nNext highest bacteria is N "+ second_highest)
diagnostics_file.write("\npopulation size: "+str(N[int(second_highest)-1]))
diagnostics_file.write("\nWith enzyme kinetics:")
diagnostics_file.write("\n\nconversion:"+str(conv[int(second_highest)-1]))
diagnostics_file.write("\nkcat: "+str(kcat[int(second_highest)-1]))
diagnostics_file.write("\nkm: "+str(km[int(second_highest)-1]))
diagnostics_file.write("\nE: "+str(E[int(second_highest)-1]))
diagnostics_file.write("\ntoxicity: "+str(tox[int(second_highest)-1]))
diagnostics_file.write("\n\nwhole ending population is: "+str(bac_tuples))
diagnostics_file.write("\nwhole ending enzyme allocation is: \n"+str(E))

diagnostics_file.close()