#!/usr/bin/env python
from __future__ import division

import csv
from pulp import *
import scipy
import numpy
from progressbar import ProgressBar
import random
random.seed(sys.argv[1])

timestep= 5 #how many timesteps should pass between sampling
global timelength
timelength= 10000 #how many timesteps in total to run the model for
dilution= 0.04
bacteria_num= 50
substrate_num=5
substrate_start=['1', '2', '3', '4', '5']
bacteria_start=1
functional_groups=2
bacteria_per_group=25

# # S E T  U P  C O D E # #
kcat= [[0.1 for x in range(substrate_num+1)]for x in range(bacteria_num)]
km= [[0.5 for x in range(substrate_num+1)]for x in range(bacteria_num)]
E= [[0.2 for x in range(substrate_num)]for x in range(bacteria_num)]
tox= [0.003217711158054604, 0.017169107690656004, 0.004394633945761939, 0.0008211538731596572, 0.014980858441925853, 0.01263450819703058, 0.002527492127616604, 0.0010137025430977126, 0.004177509142701945, 0.0012093244365359596, 0.010376420481607394, 0.015576741404024201, 0.0030306574214260668, 0.006083922184605799, 0.017496279258099098, 0.007088078833447094, 0.006380674763648184, 0.006187552885989654, 0.0048818868980900825, 0.008675581011183082, 0.003616592660533182, 0.004315447314684755, 0.015661018924689254, 0.0015707878472116972, 0.005548055551262385, 0.017419676263869775, 0.019452210557179824, 0.011488153186358587, 0.01014711877512477, 0.009146057990163587, 0.010627314509822272, 0.004297408464780319, 0.011224980017868494, 0.0037452494942547872, 0.000352178107182086, 0.014391416971883926, 0.00560007916705493, 0.01194586326354024, 0.004460148526667227, 0.00913056955740573, 0.00425138519215269, 0.018822028630973892, 0.0074765385058029165, 0.0052068500279638386, 0.010930212597168013, 0.017272108229166753, 0.0001378082301890255, 0.006663176290810242, 0.018069495938335383, 0.015387702612993858]
toxin_time = (3000, 5000)
antitoxin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]


#######################################
### GENERATES THE CONVERSION MATRIX ###
conv_groups = [[0.8, 0.75, 0.7000000000000001, 0.65, 0.6000000000000001] for x in range(bacteria_num)]
for i in range(0,bacteria_num):
	random_num= [random.uniform(-0.05,0.05) for x in range(bacteria_num)]
	for j in range(0,substrate_num):
		conv_groups[i][j]= conv_groups[i][j]+random_num[i]
		if (conv_groups[i][j]<0):
			conv_groups[i][j]=0
		if (conv_groups[i][j]>1):
			conv_groups[i][j]=1

conv=[[0 for x in range(substrate_num)]for x in range(bacteria_num)]
for h in range(0,int(bacteria_per_group)):
	for i in range(0,bacteria_num):
		for j in range(0,substrate_num):
			conv[i][j]=conv_groups[h][j]+random.uniform(-0.05,0.05)

#######################################

toxin_0=0
toxin_1=0.019
toxin= toxin_0

S1_0=1
S1=S1_0
S2_0=2
S2=S2_0
S3_0=3
S3=S3_0
S4_0=4
S4=S4_0
S5_0=5
S5=S5_0

###FUNCTIONS###


def bacteria_1(N1,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria1= N1+(conv[0][0]*(kcat[0][0]*E[0][0]*S1/(km[0][0]+S1))*N1)+(conv[0][1]*(kcat[0][1]*E[0][1]*S2/(km[0][1]+S2))*N1)+(conv[0][2]*(kcat[0][2]*E[0][2]*S3/(km[0][2]+S3))*N1)+(conv[0][3]*(kcat[0][3]*E[0][3]*S4/(km[0][3]+S4))*N1)+(conv[0][4]*(kcat[0][4]*E[0][4]*S5/(km[0][4]+S5))*N1)-(dilution*N1)-(N1*tox[0]*toxin)+(kcat[0][5]*antitoxin[0]*toxin/(km[0][5]+toxin))
	return (Bacteria1)

def bacteria_2(N2,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria2= N2+(conv[1][0]*(kcat[1][0]*E[1][0]*S1/(km[1][0]+S1))*N2)+(conv[1][1]*(kcat[1][1]*E[1][1]*S2/(km[1][1]+S2))*N2)+(conv[1][2]*(kcat[1][2]*E[1][2]*S3/(km[1][2]+S3))*N2)+(conv[1][3]*(kcat[1][3]*E[1][3]*S4/(km[1][3]+S4))*N2)+(conv[1][4]*(kcat[1][4]*E[1][4]*S5/(km[1][4]+S5))*N2)-(dilution*N2)-(N2*tox[1]*toxin)+(kcat[1][5]*antitoxin[1]*toxin/(km[1][5]+toxin))
	return (Bacteria2)

def bacteria_3(N3,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria3= N3+(conv[2][0]*(kcat[2][0]*E[2][0]*S1/(km[2][0]+S1))*N3)+(conv[2][1]*(kcat[2][1]*E[2][1]*S2/(km[2][1]+S2))*N3)+(conv[2][2]*(kcat[2][2]*E[2][2]*S3/(km[2][2]+S3))*N3)+(conv[2][3]*(kcat[2][3]*E[2][3]*S4/(km[2][3]+S4))*N3)+(conv[2][4]*(kcat[2][4]*E[2][4]*S5/(km[2][4]+S5))*N3)-(dilution*N3)-(N3*tox[2]*toxin)+(kcat[2][5]*antitoxin[2]*toxin/(km[2][5]+toxin))
	return (Bacteria3)

def bacteria_4(N4,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria4= N4+(conv[3][0]*(kcat[3][0]*E[3][0]*S1/(km[3][0]+S1))*N4)+(conv[3][1]*(kcat[3][1]*E[3][1]*S2/(km[3][1]+S2))*N4)+(conv[3][2]*(kcat[3][2]*E[3][2]*S3/(km[3][2]+S3))*N4)+(conv[3][3]*(kcat[3][3]*E[3][3]*S4/(km[3][3]+S4))*N4)+(conv[3][4]*(kcat[3][4]*E[3][4]*S5/(km[3][4]+S5))*N4)-(dilution*N4)-(N4*tox[3]*toxin)+(kcat[3][5]*antitoxin[3]*toxin/(km[3][5]+toxin))
	return (Bacteria4)

def bacteria_5(N5,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria5= N5+(conv[4][0]*(kcat[4][0]*E[4][0]*S1/(km[4][0]+S1))*N5)+(conv[4][1]*(kcat[4][1]*E[4][1]*S2/(km[4][1]+S2))*N5)+(conv[4][2]*(kcat[4][2]*E[4][2]*S3/(km[4][2]+S3))*N5)+(conv[4][3]*(kcat[4][3]*E[4][3]*S4/(km[4][3]+S4))*N5)+(conv[4][4]*(kcat[4][4]*E[4][4]*S5/(km[4][4]+S5))*N5)-(dilution*N5)-(N5*tox[4]*toxin)+(kcat[4][5]*antitoxin[4]*toxin/(km[4][5]+toxin))
	return (Bacteria5)

def bacteria_6(N6,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria6= N6+(conv[5][0]*(kcat[5][0]*E[5][0]*S1/(km[5][0]+S1))*N6)+(conv[5][1]*(kcat[5][1]*E[5][1]*S2/(km[5][1]+S2))*N6)+(conv[5][2]*(kcat[5][2]*E[5][2]*S3/(km[5][2]+S3))*N6)+(conv[5][3]*(kcat[5][3]*E[5][3]*S4/(km[5][3]+S4))*N6)+(conv[5][4]*(kcat[5][4]*E[5][4]*S5/(km[5][4]+S5))*N6)-(dilution*N6)-(N6*tox[5]*toxin)+(kcat[5][5]*antitoxin[5]*toxin/(km[5][5]+toxin))
	return (Bacteria6)

def bacteria_7(N7,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria7= N7+(conv[6][0]*(kcat[6][0]*E[6][0]*S1/(km[6][0]+S1))*N7)+(conv[6][1]*(kcat[6][1]*E[6][1]*S2/(km[6][1]+S2))*N7)+(conv[6][2]*(kcat[6][2]*E[6][2]*S3/(km[6][2]+S3))*N7)+(conv[6][3]*(kcat[6][3]*E[6][3]*S4/(km[6][3]+S4))*N7)+(conv[6][4]*(kcat[6][4]*E[6][4]*S5/(km[6][4]+S5))*N7)-(dilution*N7)-(N7*tox[6]*toxin)+(kcat[6][5]*antitoxin[6]*toxin/(km[6][5]+toxin))
	return (Bacteria7)

def bacteria_8(N8,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria8= N8+(conv[7][0]*(kcat[7][0]*E[7][0]*S1/(km[7][0]+S1))*N8)+(conv[7][1]*(kcat[7][1]*E[7][1]*S2/(km[7][1]+S2))*N8)+(conv[7][2]*(kcat[7][2]*E[7][2]*S3/(km[7][2]+S3))*N8)+(conv[7][3]*(kcat[7][3]*E[7][3]*S4/(km[7][3]+S4))*N8)+(conv[7][4]*(kcat[7][4]*E[7][4]*S5/(km[7][4]+S5))*N8)-(dilution*N8)-(N8*tox[7]*toxin)+(kcat[7][5]*antitoxin[7]*toxin/(km[7][5]+toxin))
	return (Bacteria8)

def bacteria_9(N9,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria9= N9+(conv[8][0]*(kcat[8][0]*E[8][0]*S1/(km[8][0]+S1))*N9)+(conv[8][1]*(kcat[8][1]*E[8][1]*S2/(km[8][1]+S2))*N9)+(conv[8][2]*(kcat[8][2]*E[8][2]*S3/(km[8][2]+S3))*N9)+(conv[8][3]*(kcat[8][3]*E[8][3]*S4/(km[8][3]+S4))*N9)+(conv[8][4]*(kcat[8][4]*E[8][4]*S5/(km[8][4]+S5))*N9)-(dilution*N9)-(N9*tox[8]*toxin)+(kcat[8][5]*antitoxin[8]*toxin/(km[8][5]+toxin))
	return (Bacteria9)

def bacteria_10(N10,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria10= N10+(conv[9][0]*(kcat[9][0]*E[9][0]*S1/(km[9][0]+S1))*N10)+(conv[9][1]*(kcat[9][1]*E[9][1]*S2/(km[9][1]+S2))*N10)+(conv[9][2]*(kcat[9][2]*E[9][2]*S3/(km[9][2]+S3))*N10)+(conv[9][3]*(kcat[9][3]*E[9][3]*S4/(km[9][3]+S4))*N10)+(conv[9][4]*(kcat[9][4]*E[9][4]*S5/(km[9][4]+S5))*N10)-(dilution*N10)-(N10*tox[9]*toxin)+(kcat[9][5]*antitoxin[9]*toxin/(km[9][5]+toxin))
	return (Bacteria10)

def bacteria_11(N11,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria11= N11+(conv[10][0]*(kcat[10][0]*E[10][0]*S1/(km[10][0]+S1))*N11)+(conv[10][1]*(kcat[10][1]*E[10][1]*S2/(km[10][1]+S2))*N11)+(conv[10][2]*(kcat[10][2]*E[10][2]*S3/(km[10][2]+S3))*N11)+(conv[10][3]*(kcat[10][3]*E[10][3]*S4/(km[10][3]+S4))*N11)+(conv[10][4]*(kcat[10][4]*E[10][4]*S5/(km[10][4]+S5))*N11)-(dilution*N11)-(N11*tox[10]*toxin)+(kcat[10][5]*antitoxin[10]*toxin/(km[10][5]+toxin))
	return (Bacteria11)

def bacteria_12(N12,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria12= N12+(conv[11][0]*(kcat[11][0]*E[11][0]*S1/(km[11][0]+S1))*N12)+(conv[11][1]*(kcat[11][1]*E[11][1]*S2/(km[11][1]+S2))*N12)+(conv[11][2]*(kcat[11][2]*E[11][2]*S3/(km[11][2]+S3))*N12)+(conv[11][3]*(kcat[11][3]*E[11][3]*S4/(km[11][3]+S4))*N12)+(conv[11][4]*(kcat[11][4]*E[11][4]*S5/(km[11][4]+S5))*N12)-(dilution*N12)-(N12*tox[11]*toxin)+(kcat[11][5]*antitoxin[11]*toxin/(km[11][5]+toxin))
	return (Bacteria12)

def bacteria_13(N13,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria13= N13+(conv[12][0]*(kcat[12][0]*E[12][0]*S1/(km[12][0]+S1))*N13)+(conv[12][1]*(kcat[12][1]*E[12][1]*S2/(km[12][1]+S2))*N13)+(conv[12][2]*(kcat[12][2]*E[12][2]*S3/(km[12][2]+S3))*N13)+(conv[12][3]*(kcat[12][3]*E[12][3]*S4/(km[12][3]+S4))*N13)+(conv[12][4]*(kcat[12][4]*E[12][4]*S5/(km[12][4]+S5))*N13)-(dilution*N13)-(N13*tox[12]*toxin)+(kcat[12][5]*antitoxin[12]*toxin/(km[12][5]+toxin))
	return (Bacteria13)

def bacteria_14(N14,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria14= N14+(conv[13][0]*(kcat[13][0]*E[13][0]*S1/(km[13][0]+S1))*N14)+(conv[13][1]*(kcat[13][1]*E[13][1]*S2/(km[13][1]+S2))*N14)+(conv[13][2]*(kcat[13][2]*E[13][2]*S3/(km[13][2]+S3))*N14)+(conv[13][3]*(kcat[13][3]*E[13][3]*S4/(km[13][3]+S4))*N14)+(conv[13][4]*(kcat[13][4]*E[13][4]*S5/(km[13][4]+S5))*N14)-(dilution*N14)-(N14*tox[13]*toxin)+(kcat[13][5]*antitoxin[13]*toxin/(km[13][5]+toxin))
	return (Bacteria14)

def bacteria_15(N15,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria15= N15+(conv[14][0]*(kcat[14][0]*E[14][0]*S1/(km[14][0]+S1))*N15)+(conv[14][1]*(kcat[14][1]*E[14][1]*S2/(km[14][1]+S2))*N15)+(conv[14][2]*(kcat[14][2]*E[14][2]*S3/(km[14][2]+S3))*N15)+(conv[14][3]*(kcat[14][3]*E[14][3]*S4/(km[14][3]+S4))*N15)+(conv[14][4]*(kcat[14][4]*E[14][4]*S5/(km[14][4]+S5))*N15)-(dilution*N15)-(N15*tox[14]*toxin)+(kcat[14][5]*antitoxin[14]*toxin/(km[14][5]+toxin))
	return (Bacteria15)

def bacteria_16(N16,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria16= N16+(conv[15][0]*(kcat[15][0]*E[15][0]*S1/(km[15][0]+S1))*N16)+(conv[15][1]*(kcat[15][1]*E[15][1]*S2/(km[15][1]+S2))*N16)+(conv[15][2]*(kcat[15][2]*E[15][2]*S3/(km[15][2]+S3))*N16)+(conv[15][3]*(kcat[15][3]*E[15][3]*S4/(km[15][3]+S4))*N16)+(conv[15][4]*(kcat[15][4]*E[15][4]*S5/(km[15][4]+S5))*N16)-(dilution*N16)-(N16*tox[15]*toxin)+(kcat[15][5]*antitoxin[15]*toxin/(km[15][5]+toxin))
	return (Bacteria16)

def bacteria_17(N17,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria17= N17+(conv[16][0]*(kcat[16][0]*E[16][0]*S1/(km[16][0]+S1))*N17)+(conv[16][1]*(kcat[16][1]*E[16][1]*S2/(km[16][1]+S2))*N17)+(conv[16][2]*(kcat[16][2]*E[16][2]*S3/(km[16][2]+S3))*N17)+(conv[16][3]*(kcat[16][3]*E[16][3]*S4/(km[16][3]+S4))*N17)+(conv[16][4]*(kcat[16][4]*E[16][4]*S5/(km[16][4]+S5))*N17)-(dilution*N17)-(N17*tox[16]*toxin)+(kcat[16][5]*antitoxin[16]*toxin/(km[16][5]+toxin))
	return (Bacteria17)

def bacteria_18(N18,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria18= N18+(conv[17][0]*(kcat[17][0]*E[17][0]*S1/(km[17][0]+S1))*N18)+(conv[17][1]*(kcat[17][1]*E[17][1]*S2/(km[17][1]+S2))*N18)+(conv[17][2]*(kcat[17][2]*E[17][2]*S3/(km[17][2]+S3))*N18)+(conv[17][3]*(kcat[17][3]*E[17][3]*S4/(km[17][3]+S4))*N18)+(conv[17][4]*(kcat[17][4]*E[17][4]*S5/(km[17][4]+S5))*N18)-(dilution*N18)-(N18*tox[17]*toxin)+(kcat[17][5]*antitoxin[17]*toxin/(km[17][5]+toxin))
	return (Bacteria18)

def bacteria_19(N19,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria19= N19+(conv[18][0]*(kcat[18][0]*E[18][0]*S1/(km[18][0]+S1))*N19)+(conv[18][1]*(kcat[18][1]*E[18][1]*S2/(km[18][1]+S2))*N19)+(conv[18][2]*(kcat[18][2]*E[18][2]*S3/(km[18][2]+S3))*N19)+(conv[18][3]*(kcat[18][3]*E[18][3]*S4/(km[18][3]+S4))*N19)+(conv[18][4]*(kcat[18][4]*E[18][4]*S5/(km[18][4]+S5))*N19)-(dilution*N19)-(N19*tox[18]*toxin)+(kcat[18][5]*antitoxin[18]*toxin/(km[18][5]+toxin))
	return (Bacteria19)

def bacteria_20(N20,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria20= N20+(conv[19][0]*(kcat[19][0]*E[19][0]*S1/(km[19][0]+S1))*N20)+(conv[19][1]*(kcat[19][1]*E[19][1]*S2/(km[19][1]+S2))*N20)+(conv[19][2]*(kcat[19][2]*E[19][2]*S3/(km[19][2]+S3))*N20)+(conv[19][3]*(kcat[19][3]*E[19][3]*S4/(km[19][3]+S4))*N20)+(conv[19][4]*(kcat[19][4]*E[19][4]*S5/(km[19][4]+S5))*N20)-(dilution*N20)-(N20*tox[19]*toxin)+(kcat[19][5]*antitoxin[19]*toxin/(km[19][5]+toxin))
	return (Bacteria20)

def bacteria_21(N21,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria21= N21+(conv[20][0]*(kcat[20][0]*E[20][0]*S1/(km[20][0]+S1))*N21)+(conv[20][1]*(kcat[20][1]*E[20][1]*S2/(km[20][1]+S2))*N21)+(conv[20][2]*(kcat[20][2]*E[20][2]*S3/(km[20][2]+S3))*N21)+(conv[20][3]*(kcat[20][3]*E[20][3]*S4/(km[20][3]+S4))*N21)+(conv[20][4]*(kcat[20][4]*E[20][4]*S5/(km[20][4]+S5))*N21)-(dilution*N21)-(N21*tox[20]*toxin)+(kcat[20][5]*antitoxin[20]*toxin/(km[20][5]+toxin))
	return (Bacteria21)

def bacteria_22(N22,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria22= N22+(conv[21][0]*(kcat[21][0]*E[21][0]*S1/(km[21][0]+S1))*N22)+(conv[21][1]*(kcat[21][1]*E[21][1]*S2/(km[21][1]+S2))*N22)+(conv[21][2]*(kcat[21][2]*E[21][2]*S3/(km[21][2]+S3))*N22)+(conv[21][3]*(kcat[21][3]*E[21][3]*S4/(km[21][3]+S4))*N22)+(conv[21][4]*(kcat[21][4]*E[21][4]*S5/(km[21][4]+S5))*N22)-(dilution*N22)-(N22*tox[21]*toxin)+(kcat[21][5]*antitoxin[21]*toxin/(km[21][5]+toxin))
	return (Bacteria22)

def bacteria_23(N23,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria23= N23+(conv[22][0]*(kcat[22][0]*E[22][0]*S1/(km[22][0]+S1))*N23)+(conv[22][1]*(kcat[22][1]*E[22][1]*S2/(km[22][1]+S2))*N23)+(conv[22][2]*(kcat[22][2]*E[22][2]*S3/(km[22][2]+S3))*N23)+(conv[22][3]*(kcat[22][3]*E[22][3]*S4/(km[22][3]+S4))*N23)+(conv[22][4]*(kcat[22][4]*E[22][4]*S5/(km[22][4]+S5))*N23)-(dilution*N23)-(N23*tox[22]*toxin)+(kcat[22][5]*antitoxin[22]*toxin/(km[22][5]+toxin))
	return (Bacteria23)

def bacteria_24(N24,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria24= N24+(conv[23][0]*(kcat[23][0]*E[23][0]*S1/(km[23][0]+S1))*N24)+(conv[23][1]*(kcat[23][1]*E[23][1]*S2/(km[23][1]+S2))*N24)+(conv[23][2]*(kcat[23][2]*E[23][2]*S3/(km[23][2]+S3))*N24)+(conv[23][3]*(kcat[23][3]*E[23][3]*S4/(km[23][3]+S4))*N24)+(conv[23][4]*(kcat[23][4]*E[23][4]*S5/(km[23][4]+S5))*N24)-(dilution*N24)-(N24*tox[23]*toxin)+(kcat[23][5]*antitoxin[23]*toxin/(km[23][5]+toxin))
	return (Bacteria24)

def bacteria_25(N25,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria25= N25+(conv[24][0]*(kcat[24][0]*E[24][0]*S1/(km[24][0]+S1))*N25)+(conv[24][1]*(kcat[24][1]*E[24][1]*S2/(km[24][1]+S2))*N25)+(conv[24][2]*(kcat[24][2]*E[24][2]*S3/(km[24][2]+S3))*N25)+(conv[24][3]*(kcat[24][3]*E[24][3]*S4/(km[24][3]+S4))*N25)+(conv[24][4]*(kcat[24][4]*E[24][4]*S5/(km[24][4]+S5))*N25)-(dilution*N25)-(N25*tox[24]*toxin)+(kcat[24][5]*antitoxin[24]*toxin/(km[24][5]+toxin))
	return (Bacteria25)

def bacteria_26(N26,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria26= N26+(conv[25][0]*(kcat[25][0]*E[25][0]*S1/(km[25][0]+S1))*N26)+(conv[25][1]*(kcat[25][1]*E[25][1]*S2/(km[25][1]+S2))*N26)+(conv[25][2]*(kcat[25][2]*E[25][2]*S3/(km[25][2]+S3))*N26)+(conv[25][3]*(kcat[25][3]*E[25][3]*S4/(km[25][3]+S4))*N26)+(conv[25][4]*(kcat[25][4]*E[25][4]*S5/(km[25][4]+S5))*N26)-(dilution*N26)-(N26*tox[25]*toxin)+(kcat[25][5]*antitoxin[25]*toxin/(km[25][5]+toxin))
	return (Bacteria26)

def bacteria_27(N27,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria27= N27+(conv[26][0]*(kcat[26][0]*E[26][0]*S1/(km[26][0]+S1))*N27)+(conv[26][1]*(kcat[26][1]*E[26][1]*S2/(km[26][1]+S2))*N27)+(conv[26][2]*(kcat[26][2]*E[26][2]*S3/(km[26][2]+S3))*N27)+(conv[26][3]*(kcat[26][3]*E[26][3]*S4/(km[26][3]+S4))*N27)+(conv[26][4]*(kcat[26][4]*E[26][4]*S5/(km[26][4]+S5))*N27)-(dilution*N27)-(N27*tox[26]*toxin)+(kcat[26][5]*antitoxin[26]*toxin/(km[26][5]+toxin))
	return (Bacteria27)

def bacteria_28(N28,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria28= N28+(conv[27][0]*(kcat[27][0]*E[27][0]*S1/(km[27][0]+S1))*N28)+(conv[27][1]*(kcat[27][1]*E[27][1]*S2/(km[27][1]+S2))*N28)+(conv[27][2]*(kcat[27][2]*E[27][2]*S3/(km[27][2]+S3))*N28)+(conv[27][3]*(kcat[27][3]*E[27][3]*S4/(km[27][3]+S4))*N28)+(conv[27][4]*(kcat[27][4]*E[27][4]*S5/(km[27][4]+S5))*N28)-(dilution*N28)-(N28*tox[27]*toxin)+(kcat[27][5]*antitoxin[27]*toxin/(km[27][5]+toxin))
	return (Bacteria28)

def bacteria_29(N29,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria29= N29+(conv[28][0]*(kcat[28][0]*E[28][0]*S1/(km[28][0]+S1))*N29)+(conv[28][1]*(kcat[28][1]*E[28][1]*S2/(km[28][1]+S2))*N29)+(conv[28][2]*(kcat[28][2]*E[28][2]*S3/(km[28][2]+S3))*N29)+(conv[28][3]*(kcat[28][3]*E[28][3]*S4/(km[28][3]+S4))*N29)+(conv[28][4]*(kcat[28][4]*E[28][4]*S5/(km[28][4]+S5))*N29)-(dilution*N29)-(N29*tox[28]*toxin)+(kcat[28][5]*antitoxin[28]*toxin/(km[28][5]+toxin))
	return (Bacteria29)

def bacteria_30(N30,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria30= N30+(conv[29][0]*(kcat[29][0]*E[29][0]*S1/(km[29][0]+S1))*N30)+(conv[29][1]*(kcat[29][1]*E[29][1]*S2/(km[29][1]+S2))*N30)+(conv[29][2]*(kcat[29][2]*E[29][2]*S3/(km[29][2]+S3))*N30)+(conv[29][3]*(kcat[29][3]*E[29][3]*S4/(km[29][3]+S4))*N30)+(conv[29][4]*(kcat[29][4]*E[29][4]*S5/(km[29][4]+S5))*N30)-(dilution*N30)-(N30*tox[29]*toxin)+(kcat[29][5]*antitoxin[29]*toxin/(km[29][5]+toxin))
	return (Bacteria30)

def bacteria_31(N31,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria31= N31+(conv[30][0]*(kcat[30][0]*E[30][0]*S1/(km[30][0]+S1))*N31)+(conv[30][1]*(kcat[30][1]*E[30][1]*S2/(km[30][1]+S2))*N31)+(conv[30][2]*(kcat[30][2]*E[30][2]*S3/(km[30][2]+S3))*N31)+(conv[30][3]*(kcat[30][3]*E[30][3]*S4/(km[30][3]+S4))*N31)+(conv[30][4]*(kcat[30][4]*E[30][4]*S5/(km[30][4]+S5))*N31)-(dilution*N31)-(N31*tox[30]*toxin)+(kcat[30][5]*antitoxin[30]*toxin/(km[30][5]+toxin))
	return (Bacteria31)

def bacteria_32(N32,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria32= N32+(conv[31][0]*(kcat[31][0]*E[31][0]*S1/(km[31][0]+S1))*N32)+(conv[31][1]*(kcat[31][1]*E[31][1]*S2/(km[31][1]+S2))*N32)+(conv[31][2]*(kcat[31][2]*E[31][2]*S3/(km[31][2]+S3))*N32)+(conv[31][3]*(kcat[31][3]*E[31][3]*S4/(km[31][3]+S4))*N32)+(conv[31][4]*(kcat[31][4]*E[31][4]*S5/(km[31][4]+S5))*N32)-(dilution*N32)-(N32*tox[31]*toxin)+(kcat[31][5]*antitoxin[31]*toxin/(km[31][5]+toxin))
	return (Bacteria32)

def bacteria_33(N33,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria33= N33+(conv[32][0]*(kcat[32][0]*E[32][0]*S1/(km[32][0]+S1))*N33)+(conv[32][1]*(kcat[32][1]*E[32][1]*S2/(km[32][1]+S2))*N33)+(conv[32][2]*(kcat[32][2]*E[32][2]*S3/(km[32][2]+S3))*N33)+(conv[32][3]*(kcat[32][3]*E[32][3]*S4/(km[32][3]+S4))*N33)+(conv[32][4]*(kcat[32][4]*E[32][4]*S5/(km[32][4]+S5))*N33)-(dilution*N33)-(N33*tox[32]*toxin)+(kcat[32][5]*antitoxin[32]*toxin/(km[32][5]+toxin))
	return (Bacteria33)

def bacteria_34(N34,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria34= N34+(conv[33][0]*(kcat[33][0]*E[33][0]*S1/(km[33][0]+S1))*N34)+(conv[33][1]*(kcat[33][1]*E[33][1]*S2/(km[33][1]+S2))*N34)+(conv[33][2]*(kcat[33][2]*E[33][2]*S3/(km[33][2]+S3))*N34)+(conv[33][3]*(kcat[33][3]*E[33][3]*S4/(km[33][3]+S4))*N34)+(conv[33][4]*(kcat[33][4]*E[33][4]*S5/(km[33][4]+S5))*N34)-(dilution*N34)-(N34*tox[33]*toxin)+(kcat[33][5]*antitoxin[33]*toxin/(km[33][5]+toxin))
	return (Bacteria34)

def bacteria_35(N35,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria35= N35+(conv[34][0]*(kcat[34][0]*E[34][0]*S1/(km[34][0]+S1))*N35)+(conv[34][1]*(kcat[34][1]*E[34][1]*S2/(km[34][1]+S2))*N35)+(conv[34][2]*(kcat[34][2]*E[34][2]*S3/(km[34][2]+S3))*N35)+(conv[34][3]*(kcat[34][3]*E[34][3]*S4/(km[34][3]+S4))*N35)+(conv[34][4]*(kcat[34][4]*E[34][4]*S5/(km[34][4]+S5))*N35)-(dilution*N35)-(N35*tox[34]*toxin)+(kcat[34][5]*antitoxin[34]*toxin/(km[34][5]+toxin))
	return (Bacteria35)

def bacteria_36(N36,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria36= N36+(conv[35][0]*(kcat[35][0]*E[35][0]*S1/(km[35][0]+S1))*N36)+(conv[35][1]*(kcat[35][1]*E[35][1]*S2/(km[35][1]+S2))*N36)+(conv[35][2]*(kcat[35][2]*E[35][2]*S3/(km[35][2]+S3))*N36)+(conv[35][3]*(kcat[35][3]*E[35][3]*S4/(km[35][3]+S4))*N36)+(conv[35][4]*(kcat[35][4]*E[35][4]*S5/(km[35][4]+S5))*N36)-(dilution*N36)-(N36*tox[35]*toxin)+(kcat[35][5]*antitoxin[35]*toxin/(km[35][5]+toxin))
	return (Bacteria36)

def bacteria_37(N37,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria37= N37+(conv[36][0]*(kcat[36][0]*E[36][0]*S1/(km[36][0]+S1))*N37)+(conv[36][1]*(kcat[36][1]*E[36][1]*S2/(km[36][1]+S2))*N37)+(conv[36][2]*(kcat[36][2]*E[36][2]*S3/(km[36][2]+S3))*N37)+(conv[36][3]*(kcat[36][3]*E[36][3]*S4/(km[36][3]+S4))*N37)+(conv[36][4]*(kcat[36][4]*E[36][4]*S5/(km[36][4]+S5))*N37)-(dilution*N37)-(N37*tox[36]*toxin)+(kcat[36][5]*antitoxin[36]*toxin/(km[36][5]+toxin))
	return (Bacteria37)

def bacteria_38(N38,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria38= N38+(conv[37][0]*(kcat[37][0]*E[37][0]*S1/(km[37][0]+S1))*N38)+(conv[37][1]*(kcat[37][1]*E[37][1]*S2/(km[37][1]+S2))*N38)+(conv[37][2]*(kcat[37][2]*E[37][2]*S3/(km[37][2]+S3))*N38)+(conv[37][3]*(kcat[37][3]*E[37][3]*S4/(km[37][3]+S4))*N38)+(conv[37][4]*(kcat[37][4]*E[37][4]*S5/(km[37][4]+S5))*N38)-(dilution*N38)-(N38*tox[37]*toxin)+(kcat[37][5]*antitoxin[37]*toxin/(km[37][5]+toxin))
	return (Bacteria38)

def bacteria_39(N39,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria39= N39+(conv[38][0]*(kcat[38][0]*E[38][0]*S1/(km[38][0]+S1))*N39)+(conv[38][1]*(kcat[38][1]*E[38][1]*S2/(km[38][1]+S2))*N39)+(conv[38][2]*(kcat[38][2]*E[38][2]*S3/(km[38][2]+S3))*N39)+(conv[38][3]*(kcat[38][3]*E[38][3]*S4/(km[38][3]+S4))*N39)+(conv[38][4]*(kcat[38][4]*E[38][4]*S5/(km[38][4]+S5))*N39)-(dilution*N39)-(N39*tox[38]*toxin)+(kcat[38][5]*antitoxin[38]*toxin/(km[38][5]+toxin))
	return (Bacteria39)

def bacteria_40(N40,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria40= N40+(conv[39][0]*(kcat[39][0]*E[39][0]*S1/(km[39][0]+S1))*N40)+(conv[39][1]*(kcat[39][1]*E[39][1]*S2/(km[39][1]+S2))*N40)+(conv[39][2]*(kcat[39][2]*E[39][2]*S3/(km[39][2]+S3))*N40)+(conv[39][3]*(kcat[39][3]*E[39][3]*S4/(km[39][3]+S4))*N40)+(conv[39][4]*(kcat[39][4]*E[39][4]*S5/(km[39][4]+S5))*N40)-(dilution*N40)-(N40*tox[39]*toxin)+(kcat[39][5]*antitoxin[39]*toxin/(km[39][5]+toxin))
	return (Bacteria40)

def bacteria_41(N41,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria41= N41+(conv[40][0]*(kcat[40][0]*E[40][0]*S1/(km[40][0]+S1))*N41)+(conv[40][1]*(kcat[40][1]*E[40][1]*S2/(km[40][1]+S2))*N41)+(conv[40][2]*(kcat[40][2]*E[40][2]*S3/(km[40][2]+S3))*N41)+(conv[40][3]*(kcat[40][3]*E[40][3]*S4/(km[40][3]+S4))*N41)+(conv[40][4]*(kcat[40][4]*E[40][4]*S5/(km[40][4]+S5))*N41)-(dilution*N41)-(N41*tox[40]*toxin)+(kcat[40][5]*antitoxin[40]*toxin/(km[40][5]+toxin))
	return (Bacteria41)

def bacteria_42(N42,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria42= N42+(conv[41][0]*(kcat[41][0]*E[41][0]*S1/(km[41][0]+S1))*N42)+(conv[41][1]*(kcat[41][1]*E[41][1]*S2/(km[41][1]+S2))*N42)+(conv[41][2]*(kcat[41][2]*E[41][2]*S3/(km[41][2]+S3))*N42)+(conv[41][3]*(kcat[41][3]*E[41][3]*S4/(km[41][3]+S4))*N42)+(conv[41][4]*(kcat[41][4]*E[41][4]*S5/(km[41][4]+S5))*N42)-(dilution*N42)-(N42*tox[41]*toxin)+(kcat[41][5]*antitoxin[41]*toxin/(km[41][5]+toxin))
	return (Bacteria42)

def bacteria_43(N43,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria43= N43+(conv[42][0]*(kcat[42][0]*E[42][0]*S1/(km[42][0]+S1))*N43)+(conv[42][1]*(kcat[42][1]*E[42][1]*S2/(km[42][1]+S2))*N43)+(conv[42][2]*(kcat[42][2]*E[42][2]*S3/(km[42][2]+S3))*N43)+(conv[42][3]*(kcat[42][3]*E[42][3]*S4/(km[42][3]+S4))*N43)+(conv[42][4]*(kcat[42][4]*E[42][4]*S5/(km[42][4]+S5))*N43)-(dilution*N43)-(N43*tox[42]*toxin)+(kcat[42][5]*antitoxin[42]*toxin/(km[42][5]+toxin))
	return (Bacteria43)

def bacteria_44(N44,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria44= N44+(conv[43][0]*(kcat[43][0]*E[43][0]*S1/(km[43][0]+S1))*N44)+(conv[43][1]*(kcat[43][1]*E[43][1]*S2/(km[43][1]+S2))*N44)+(conv[43][2]*(kcat[43][2]*E[43][2]*S3/(km[43][2]+S3))*N44)+(conv[43][3]*(kcat[43][3]*E[43][3]*S4/(km[43][3]+S4))*N44)+(conv[43][4]*(kcat[43][4]*E[43][4]*S5/(km[43][4]+S5))*N44)-(dilution*N44)-(N44*tox[43]*toxin)+(kcat[43][5]*antitoxin[43]*toxin/(km[43][5]+toxin))
	return (Bacteria44)

def bacteria_45(N45,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria45= N45+(conv[44][0]*(kcat[44][0]*E[44][0]*S1/(km[44][0]+S1))*N45)+(conv[44][1]*(kcat[44][1]*E[44][1]*S2/(km[44][1]+S2))*N45)+(conv[44][2]*(kcat[44][2]*E[44][2]*S3/(km[44][2]+S3))*N45)+(conv[44][3]*(kcat[44][3]*E[44][3]*S4/(km[44][3]+S4))*N45)+(conv[44][4]*(kcat[44][4]*E[44][4]*S5/(km[44][4]+S5))*N45)-(dilution*N45)-(N45*tox[44]*toxin)+(kcat[44][5]*antitoxin[44]*toxin/(km[44][5]+toxin))
	return (Bacteria45)

def bacteria_46(N46,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria46= N46+(conv[45][0]*(kcat[45][0]*E[45][0]*S1/(km[45][0]+S1))*N46)+(conv[45][1]*(kcat[45][1]*E[45][1]*S2/(km[45][1]+S2))*N46)+(conv[45][2]*(kcat[45][2]*E[45][2]*S3/(km[45][2]+S3))*N46)+(conv[45][3]*(kcat[45][3]*E[45][3]*S4/(km[45][3]+S4))*N46)+(conv[45][4]*(kcat[45][4]*E[45][4]*S5/(km[45][4]+S5))*N46)-(dilution*N46)-(N46*tox[45]*toxin)+(kcat[45][5]*antitoxin[45]*toxin/(km[45][5]+toxin))
	return (Bacteria46)

def bacteria_47(N47,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria47= N47+(conv[46][0]*(kcat[46][0]*E[46][0]*S1/(km[46][0]+S1))*N47)+(conv[46][1]*(kcat[46][1]*E[46][1]*S2/(km[46][1]+S2))*N47)+(conv[46][2]*(kcat[46][2]*E[46][2]*S3/(km[46][2]+S3))*N47)+(conv[46][3]*(kcat[46][3]*E[46][3]*S4/(km[46][3]+S4))*N47)+(conv[46][4]*(kcat[46][4]*E[46][4]*S5/(km[46][4]+S5))*N47)-(dilution*N47)-(N47*tox[46]*toxin)+(kcat[46][5]*antitoxin[46]*toxin/(km[46][5]+toxin))
	return (Bacteria47)

def bacteria_48(N48,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria48= N48+(conv[47][0]*(kcat[47][0]*E[47][0]*S1/(km[47][0]+S1))*N48)+(conv[47][1]*(kcat[47][1]*E[47][1]*S2/(km[47][1]+S2))*N48)+(conv[47][2]*(kcat[47][2]*E[47][2]*S3/(km[47][2]+S3))*N48)+(conv[47][3]*(kcat[47][3]*E[47][3]*S4/(km[47][3]+S4))*N48)+(conv[47][4]*(kcat[47][4]*E[47][4]*S5/(km[47][4]+S5))*N48)-(dilution*N48)-(N48*tox[47]*toxin)+(kcat[47][5]*antitoxin[47]*toxin/(km[47][5]+toxin))
	return (Bacteria48)

def bacteria_49(N49,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria49= N49+(conv[48][0]*(kcat[48][0]*E[48][0]*S1/(km[48][0]+S1))*N49)+(conv[48][1]*(kcat[48][1]*E[48][1]*S2/(km[48][1]+S2))*N49)+(conv[48][2]*(kcat[48][2]*E[48][2]*S3/(km[48][2]+S3))*N49)+(conv[48][3]*(kcat[48][3]*E[48][3]*S4/(km[48][3]+S4))*N49)+(conv[48][4]*(kcat[48][4]*E[48][4]*S5/(km[48][4]+S5))*N49)-(dilution*N49)-(N49*tox[48]*toxin)+(kcat[48][5]*antitoxin[48]*toxin/(km[48][5]+toxin))
	return (Bacteria49)

def bacteria_50(N50,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5):
	Bacteria50= N50+(conv[49][0]*(kcat[49][0]*E[49][0]*S1/(km[49][0]+S1))*N50)+(conv[49][1]*(kcat[49][1]*E[49][1]*S2/(km[49][1]+S2))*N50)+(conv[49][2]*(kcat[49][2]*E[49][2]*S3/(km[49][2]+S3))*N50)+(conv[49][3]*(kcat[49][3]*E[49][3]*S4/(km[49][3]+S4))*N50)+(conv[49][4]*(kcat[49][4]*E[49][4]*S5/(km[49][4]+S5))*N50)-(dilution*N50)-(N50*tox[49]*toxin)+(kcat[49][5]*antitoxin[49]*toxin/(km[49][5]+toxin))
	return (Bacteria50)

def substrate_1(S1,kcat,conv,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50):
	Substrate1=S1-((kcat[0][0]*E[0][0]*S1/(km[0][0]+S1))*N1)-((kcat[1][0]*E[1][0]*S1/(km[1][0]+S1))*N2)-((kcat[2][0]*E[2][0]*S1/(km[2][0]+S1))*N3)-((kcat[3][0]*E[3][0]*S1/(km[3][0]+S1))*N4)-((kcat[4][0]*E[4][0]*S1/(km[4][0]+S1))*N5)-((kcat[5][0]*E[5][0]*S1/(km[5][0]+S1))*N6)-((kcat[6][0]*E[6][0]*S1/(km[6][0]+S1))*N7)-((kcat[7][0]*E[7][0]*S1/(km[7][0]+S1))*N8)-((kcat[8][0]*E[8][0]*S1/(km[8][0]+S1))*N9)-((kcat[9][0]*E[9][0]*S1/(km[9][0]+S1))*N10)-((kcat[10][0]*E[10][0]*S1/(km[10][0]+S1))*N11)-((kcat[11][0]*E[11][0]*S1/(km[11][0]+S1))*N12)-((kcat[12][0]*E[12][0]*S1/(km[12][0]+S1))*N13)-((kcat[13][0]*E[13][0]*S1/(km[13][0]+S1))*N14)-((kcat[14][0]*E[14][0]*S1/(km[14][0]+S1))*N15)-((kcat[15][0]*E[15][0]*S1/(km[15][0]+S1))*N16)-((kcat[16][0]*E[16][0]*S1/(km[16][0]+S1))*N17)-((kcat[17][0]*E[17][0]*S1/(km[17][0]+S1))*N18)-((kcat[18][0]*E[18][0]*S1/(km[18][0]+S1))*N19)-((kcat[19][0]*E[19][0]*S1/(km[19][0]+S1))*N20)-((kcat[20][0]*E[20][0]*S1/(km[20][0]+S1))*N21)-((kcat[21][0]*E[21][0]*S1/(km[21][0]+S1))*N22)-((kcat[22][0]*E[22][0]*S1/(km[22][0]+S1))*N23)-((kcat[23][0]*E[23][0]*S1/(km[23][0]+S1))*N24)-((kcat[24][0]*E[24][0]*S1/(km[24][0]+S1))*N25)-((kcat[25][0]*E[25][0]*S1/(km[25][0]+S1))*N26)-((kcat[26][0]*E[26][0]*S1/(km[26][0]+S1))*N27)-((kcat[27][0]*E[27][0]*S1/(km[27][0]+S1))*N28)-((kcat[28][0]*E[28][0]*S1/(km[28][0]+S1))*N29)-((kcat[29][0]*E[29][0]*S1/(km[29][0]+S1))*N30)-((kcat[30][0]*E[30][0]*S1/(km[30][0]+S1))*N31)-((kcat[31][0]*E[31][0]*S1/(km[31][0]+S1))*N32)-((kcat[32][0]*E[32][0]*S1/(km[32][0]+S1))*N33)-((kcat[33][0]*E[33][0]*S1/(km[33][0]+S1))*N34)-((kcat[34][0]*E[34][0]*S1/(km[34][0]+S1))*N35)-((kcat[35][0]*E[35][0]*S1/(km[35][0]+S1))*N36)-((kcat[36][0]*E[36][0]*S1/(km[36][0]+S1))*N37)-((kcat[37][0]*E[37][0]*S1/(km[37][0]+S1))*N38)-((kcat[38][0]*E[38][0]*S1/(km[38][0]+S1))*N39)-((kcat[39][0]*E[39][0]*S1/(km[39][0]+S1))*N40)-((kcat[40][0]*E[40][0]*S1/(km[40][0]+S1))*N41)-((kcat[41][0]*E[41][0]*S1/(km[41][0]+S1))*N42)-((kcat[42][0]*E[42][0]*S1/(km[42][0]+S1))*N43)-((kcat[43][0]*E[43][0]*S1/(km[43][0]+S1))*N44)-((kcat[44][0]*E[44][0]*S1/(km[44][0]+S1))*N45)-((kcat[45][0]*E[45][0]*S1/(km[45][0]+S1))*N46)-((kcat[46][0]*E[46][0]*S1/(km[46][0]+S1))*N47)-((kcat[47][0]*E[47][0]*S1/(km[47][0]+S1))*N48)-((kcat[48][0]*E[48][0]*S1/(km[48][0]+S1))*N49)-((kcat[49][0]*E[49][0]*S1/(km[49][0]+S1))*N50)-dilution*S1+dilution*S1_0
	return (Substrate1)

def substrate_2(S2,kcat,conv,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50):
	Substrate2=S2-((kcat[0][1]*E[0][1]*S2/(km[0][1]+S2))*N1)-((kcat[1][1]*E[1][1]*S2/(km[1][1]+S2))*N2)-((kcat[2][1]*E[2][1]*S2/(km[2][1]+S2))*N3)-((kcat[3][1]*E[3][1]*S2/(km[3][1]+S2))*N4)-((kcat[4][1]*E[4][1]*S2/(km[4][1]+S2))*N5)-((kcat[5][1]*E[5][1]*S2/(km[5][1]+S2))*N6)-((kcat[6][1]*E[6][1]*S2/(km[6][1]+S2))*N7)-((kcat[7][1]*E[7][1]*S2/(km[7][1]+S2))*N8)-((kcat[8][1]*E[8][1]*S2/(km[8][1]+S2))*N9)-((kcat[9][1]*E[9][1]*S2/(km[9][1]+S2))*N10)-((kcat[10][1]*E[10][1]*S2/(km[10][1]+S2))*N11)-((kcat[11][1]*E[11][1]*S2/(km[11][1]+S2))*N12)-((kcat[12][1]*E[12][1]*S2/(km[12][1]+S2))*N13)-((kcat[13][1]*E[13][1]*S2/(km[13][1]+S2))*N14)-((kcat[14][1]*E[14][1]*S2/(km[14][1]+S2))*N15)-((kcat[15][1]*E[15][1]*S2/(km[15][1]+S2))*N16)-((kcat[16][1]*E[16][1]*S2/(km[16][1]+S2))*N17)-((kcat[17][1]*E[17][1]*S2/(km[17][1]+S2))*N18)-((kcat[18][1]*E[18][1]*S2/(km[18][1]+S2))*N19)-((kcat[19][1]*E[19][1]*S2/(km[19][1]+S2))*N20)-((kcat[20][1]*E[20][1]*S2/(km[20][1]+S2))*N21)-((kcat[21][1]*E[21][1]*S2/(km[21][1]+S2))*N22)-((kcat[22][1]*E[22][1]*S2/(km[22][1]+S2))*N23)-((kcat[23][1]*E[23][1]*S2/(km[23][1]+S2))*N24)-((kcat[24][1]*E[24][1]*S2/(km[24][1]+S2))*N25)-((kcat[25][1]*E[25][1]*S2/(km[25][1]+S2))*N26)-((kcat[26][1]*E[26][1]*S2/(km[26][1]+S2))*N27)-((kcat[27][1]*E[27][1]*S2/(km[27][1]+S2))*N28)-((kcat[28][1]*E[28][1]*S2/(km[28][1]+S2))*N29)-((kcat[29][1]*E[29][1]*S2/(km[29][1]+S2))*N30)-((kcat[30][1]*E[30][1]*S2/(km[30][1]+S2))*N31)-((kcat[31][1]*E[31][1]*S2/(km[31][1]+S2))*N32)-((kcat[32][1]*E[32][1]*S2/(km[32][1]+S2))*N33)-((kcat[33][1]*E[33][1]*S2/(km[33][1]+S2))*N34)-((kcat[34][1]*E[34][1]*S2/(km[34][1]+S2))*N35)-((kcat[35][1]*E[35][1]*S2/(km[35][1]+S2))*N36)-((kcat[36][1]*E[36][1]*S2/(km[36][1]+S2))*N37)-((kcat[37][1]*E[37][1]*S2/(km[37][1]+S2))*N38)-((kcat[38][1]*E[38][1]*S2/(km[38][1]+S2))*N39)-((kcat[39][1]*E[39][1]*S2/(km[39][1]+S2))*N40)-((kcat[40][1]*E[40][1]*S2/(km[40][1]+S2))*N41)-((kcat[41][1]*E[41][1]*S2/(km[41][1]+S2))*N42)-((kcat[42][1]*E[42][1]*S2/(km[42][1]+S2))*N43)-((kcat[43][1]*E[43][1]*S2/(km[43][1]+S2))*N44)-((kcat[44][1]*E[44][1]*S2/(km[44][1]+S2))*N45)-((kcat[45][1]*E[45][1]*S2/(km[45][1]+S2))*N46)-((kcat[46][1]*E[46][1]*S2/(km[46][1]+S2))*N47)-((kcat[47][1]*E[47][1]*S2/(km[47][1]+S2))*N48)-((kcat[48][1]*E[48][1]*S2/(km[48][1]+S2))*N49)-((kcat[49][1]*E[49][1]*S2/(km[49][1]+S2))*N50)-dilution*S2+dilution*S2_0
	return (Substrate2)

def substrate_3(S3,kcat,conv,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50):
	Substrate3=S3-((kcat[0][2]*E[0][2]*S3/(km[0][2]+S3))*N1)-((kcat[1][2]*E[1][2]*S3/(km[1][2]+S3))*N2)-((kcat[2][2]*E[2][2]*S3/(km[2][2]+S3))*N3)-((kcat[3][2]*E[3][2]*S3/(km[3][2]+S3))*N4)-((kcat[4][2]*E[4][2]*S3/(km[4][2]+S3))*N5)-((kcat[5][2]*E[5][2]*S3/(km[5][2]+S3))*N6)-((kcat[6][2]*E[6][2]*S3/(km[6][2]+S3))*N7)-((kcat[7][2]*E[7][2]*S3/(km[7][2]+S3))*N8)-((kcat[8][2]*E[8][2]*S3/(km[8][2]+S3))*N9)-((kcat[9][2]*E[9][2]*S3/(km[9][2]+S3))*N10)-((kcat[10][2]*E[10][2]*S3/(km[10][2]+S3))*N11)-((kcat[11][2]*E[11][2]*S3/(km[11][2]+S3))*N12)-((kcat[12][2]*E[12][2]*S3/(km[12][2]+S3))*N13)-((kcat[13][2]*E[13][2]*S3/(km[13][2]+S3))*N14)-((kcat[14][2]*E[14][2]*S3/(km[14][2]+S3))*N15)-((kcat[15][2]*E[15][2]*S3/(km[15][2]+S3))*N16)-((kcat[16][2]*E[16][2]*S3/(km[16][2]+S3))*N17)-((kcat[17][2]*E[17][2]*S3/(km[17][2]+S3))*N18)-((kcat[18][2]*E[18][2]*S3/(km[18][2]+S3))*N19)-((kcat[19][2]*E[19][2]*S3/(km[19][2]+S3))*N20)-((kcat[20][2]*E[20][2]*S3/(km[20][2]+S3))*N21)-((kcat[21][2]*E[21][2]*S3/(km[21][2]+S3))*N22)-((kcat[22][2]*E[22][2]*S3/(km[22][2]+S3))*N23)-((kcat[23][2]*E[23][2]*S3/(km[23][2]+S3))*N24)-((kcat[24][2]*E[24][2]*S3/(km[24][2]+S3))*N25)-((kcat[25][2]*E[25][2]*S3/(km[25][2]+S3))*N26)-((kcat[26][2]*E[26][2]*S3/(km[26][2]+S3))*N27)-((kcat[27][2]*E[27][2]*S3/(km[27][2]+S3))*N28)-((kcat[28][2]*E[28][2]*S3/(km[28][2]+S3))*N29)-((kcat[29][2]*E[29][2]*S3/(km[29][2]+S3))*N30)-((kcat[30][2]*E[30][2]*S3/(km[30][2]+S3))*N31)-((kcat[31][2]*E[31][2]*S3/(km[31][2]+S3))*N32)-((kcat[32][2]*E[32][2]*S3/(km[32][2]+S3))*N33)-((kcat[33][2]*E[33][2]*S3/(km[33][2]+S3))*N34)-((kcat[34][2]*E[34][2]*S3/(km[34][2]+S3))*N35)-((kcat[35][2]*E[35][2]*S3/(km[35][2]+S3))*N36)-((kcat[36][2]*E[36][2]*S3/(km[36][2]+S3))*N37)-((kcat[37][2]*E[37][2]*S3/(km[37][2]+S3))*N38)-((kcat[38][2]*E[38][2]*S3/(km[38][2]+S3))*N39)-((kcat[39][2]*E[39][2]*S3/(km[39][2]+S3))*N40)-((kcat[40][2]*E[40][2]*S3/(km[40][2]+S3))*N41)-((kcat[41][2]*E[41][2]*S3/(km[41][2]+S3))*N42)-((kcat[42][2]*E[42][2]*S3/(km[42][2]+S3))*N43)-((kcat[43][2]*E[43][2]*S3/(km[43][2]+S3))*N44)-((kcat[44][2]*E[44][2]*S3/(km[44][2]+S3))*N45)-((kcat[45][2]*E[45][2]*S3/(km[45][2]+S3))*N46)-((kcat[46][2]*E[46][2]*S3/(km[46][2]+S3))*N47)-((kcat[47][2]*E[47][2]*S3/(km[47][2]+S3))*N48)-((kcat[48][2]*E[48][2]*S3/(km[48][2]+S3))*N49)-((kcat[49][2]*E[49][2]*S3/(km[49][2]+S3))*N50)-dilution*S3+dilution*S3_0
	return (Substrate3)

def substrate_4(S4,kcat,conv,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50):
	Substrate4=S4-((kcat[0][3]*E[0][3]*S4/(km[0][3]+S4))*N1)-((kcat[1][3]*E[1][3]*S4/(km[1][3]+S4))*N2)-((kcat[2][3]*E[2][3]*S4/(km[2][3]+S4))*N3)-((kcat[3][3]*E[3][3]*S4/(km[3][3]+S4))*N4)-((kcat[4][3]*E[4][3]*S4/(km[4][3]+S4))*N5)-((kcat[5][3]*E[5][3]*S4/(km[5][3]+S4))*N6)-((kcat[6][3]*E[6][3]*S4/(km[6][3]+S4))*N7)-((kcat[7][3]*E[7][3]*S4/(km[7][3]+S4))*N8)-((kcat[8][3]*E[8][3]*S4/(km[8][3]+S4))*N9)-((kcat[9][3]*E[9][3]*S4/(km[9][3]+S4))*N10)-((kcat[10][3]*E[10][3]*S4/(km[10][3]+S4))*N11)-((kcat[11][3]*E[11][3]*S4/(km[11][3]+S4))*N12)-((kcat[12][3]*E[12][3]*S4/(km[12][3]+S4))*N13)-((kcat[13][3]*E[13][3]*S4/(km[13][3]+S4))*N14)-((kcat[14][3]*E[14][3]*S4/(km[14][3]+S4))*N15)-((kcat[15][3]*E[15][3]*S4/(km[15][3]+S4))*N16)-((kcat[16][3]*E[16][3]*S4/(km[16][3]+S4))*N17)-((kcat[17][3]*E[17][3]*S4/(km[17][3]+S4))*N18)-((kcat[18][3]*E[18][3]*S4/(km[18][3]+S4))*N19)-((kcat[19][3]*E[19][3]*S4/(km[19][3]+S4))*N20)-((kcat[20][3]*E[20][3]*S4/(km[20][3]+S4))*N21)-((kcat[21][3]*E[21][3]*S4/(km[21][3]+S4))*N22)-((kcat[22][3]*E[22][3]*S4/(km[22][3]+S4))*N23)-((kcat[23][3]*E[23][3]*S4/(km[23][3]+S4))*N24)-((kcat[24][3]*E[24][3]*S4/(km[24][3]+S4))*N25)-((kcat[25][3]*E[25][3]*S4/(km[25][3]+S4))*N26)-((kcat[26][3]*E[26][3]*S4/(km[26][3]+S4))*N27)-((kcat[27][3]*E[27][3]*S4/(km[27][3]+S4))*N28)-((kcat[28][3]*E[28][3]*S4/(km[28][3]+S4))*N29)-((kcat[29][3]*E[29][3]*S4/(km[29][3]+S4))*N30)-((kcat[30][3]*E[30][3]*S4/(km[30][3]+S4))*N31)-((kcat[31][3]*E[31][3]*S4/(km[31][3]+S4))*N32)-((kcat[32][3]*E[32][3]*S4/(km[32][3]+S4))*N33)-((kcat[33][3]*E[33][3]*S4/(km[33][3]+S4))*N34)-((kcat[34][3]*E[34][3]*S4/(km[34][3]+S4))*N35)-((kcat[35][3]*E[35][3]*S4/(km[35][3]+S4))*N36)-((kcat[36][3]*E[36][3]*S4/(km[36][3]+S4))*N37)-((kcat[37][3]*E[37][3]*S4/(km[37][3]+S4))*N38)-((kcat[38][3]*E[38][3]*S4/(km[38][3]+S4))*N39)-((kcat[39][3]*E[39][3]*S4/(km[39][3]+S4))*N40)-((kcat[40][3]*E[40][3]*S4/(km[40][3]+S4))*N41)-((kcat[41][3]*E[41][3]*S4/(km[41][3]+S4))*N42)-((kcat[42][3]*E[42][3]*S4/(km[42][3]+S4))*N43)-((kcat[43][3]*E[43][3]*S4/(km[43][3]+S4))*N44)-((kcat[44][3]*E[44][3]*S4/(km[44][3]+S4))*N45)-((kcat[45][3]*E[45][3]*S4/(km[45][3]+S4))*N46)-((kcat[46][3]*E[46][3]*S4/(km[46][3]+S4))*N47)-((kcat[47][3]*E[47][3]*S4/(km[47][3]+S4))*N48)-((kcat[48][3]*E[48][3]*S4/(km[48][3]+S4))*N49)-((kcat[49][3]*E[49][3]*S4/(km[49][3]+S4))*N50)-dilution*S4+dilution*S4_0
	return (Substrate4)

def substrate_5(S5,kcat,conv,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50):
	Substrate5=S5-((kcat[0][4]*E[0][4]*S5/(km[0][4]+S5))*N1)-((kcat[1][4]*E[1][4]*S5/(km[1][4]+S5))*N2)-((kcat[2][4]*E[2][4]*S5/(km[2][4]+S5))*N3)-((kcat[3][4]*E[3][4]*S5/(km[3][4]+S5))*N4)-((kcat[4][4]*E[4][4]*S5/(km[4][4]+S5))*N5)-((kcat[5][4]*E[5][4]*S5/(km[5][4]+S5))*N6)-((kcat[6][4]*E[6][4]*S5/(km[6][4]+S5))*N7)-((kcat[7][4]*E[7][4]*S5/(km[7][4]+S5))*N8)-((kcat[8][4]*E[8][4]*S5/(km[8][4]+S5))*N9)-((kcat[9][4]*E[9][4]*S5/(km[9][4]+S5))*N10)-((kcat[10][4]*E[10][4]*S5/(km[10][4]+S5))*N11)-((kcat[11][4]*E[11][4]*S5/(km[11][4]+S5))*N12)-((kcat[12][4]*E[12][4]*S5/(km[12][4]+S5))*N13)-((kcat[13][4]*E[13][4]*S5/(km[13][4]+S5))*N14)-((kcat[14][4]*E[14][4]*S5/(km[14][4]+S5))*N15)-((kcat[15][4]*E[15][4]*S5/(km[15][4]+S5))*N16)-((kcat[16][4]*E[16][4]*S5/(km[16][4]+S5))*N17)-((kcat[17][4]*E[17][4]*S5/(km[17][4]+S5))*N18)-((kcat[18][4]*E[18][4]*S5/(km[18][4]+S5))*N19)-((kcat[19][4]*E[19][4]*S5/(km[19][4]+S5))*N20)-((kcat[20][4]*E[20][4]*S5/(km[20][4]+S5))*N21)-((kcat[21][4]*E[21][4]*S5/(km[21][4]+S5))*N22)-((kcat[22][4]*E[22][4]*S5/(km[22][4]+S5))*N23)-((kcat[23][4]*E[23][4]*S5/(km[23][4]+S5))*N24)-((kcat[24][4]*E[24][4]*S5/(km[24][4]+S5))*N25)-((kcat[25][4]*E[25][4]*S5/(km[25][4]+S5))*N26)-((kcat[26][4]*E[26][4]*S5/(km[26][4]+S5))*N27)-((kcat[27][4]*E[27][4]*S5/(km[27][4]+S5))*N28)-((kcat[28][4]*E[28][4]*S5/(km[28][4]+S5))*N29)-((kcat[29][4]*E[29][4]*S5/(km[29][4]+S5))*N30)-((kcat[30][4]*E[30][4]*S5/(km[30][4]+S5))*N31)-((kcat[31][4]*E[31][4]*S5/(km[31][4]+S5))*N32)-((kcat[32][4]*E[32][4]*S5/(km[32][4]+S5))*N33)-((kcat[33][4]*E[33][4]*S5/(km[33][4]+S5))*N34)-((kcat[34][4]*E[34][4]*S5/(km[34][4]+S5))*N35)-((kcat[35][4]*E[35][4]*S5/(km[35][4]+S5))*N36)-((kcat[36][4]*E[36][4]*S5/(km[36][4]+S5))*N37)-((kcat[37][4]*E[37][4]*S5/(km[37][4]+S5))*N38)-((kcat[38][4]*E[38][4]*S5/(km[38][4]+S5))*N39)-((kcat[39][4]*E[39][4]*S5/(km[39][4]+S5))*N40)-((kcat[40][4]*E[40][4]*S5/(km[40][4]+S5))*N41)-((kcat[41][4]*E[41][4]*S5/(km[41][4]+S5))*N42)-((kcat[42][4]*E[42][4]*S5/(km[42][4]+S5))*N43)-((kcat[43][4]*E[43][4]*S5/(km[43][4]+S5))*N44)-((kcat[44][4]*E[44][4]*S5/(km[44][4]+S5))*N45)-((kcat[45][4]*E[45][4]*S5/(km[45][4]+S5))*N46)-((kcat[46][4]*E[46][4]*S5/(km[46][4]+S5))*N47)-((kcat[47][4]*E[47][4]*S5/(km[47][4]+S5))*N48)-((kcat[48][4]*E[48][4]*S5/(km[48][4]+S5))*N49)-((kcat[49][4]*E[49][4]*S5/(km[49][4]+S5))*N50)-dilution*S5+dilution*S5_0
	return (Substrate5)

def toxin_eqn(toxin,toxin_1):
	toxin = toxin + toxin_1 - (toxin*dilution)
	return(toxin)

#RUNNING CODE#
samplepoints= range(0,int(timelength)) #create a vector of the number
#of times you want to sample the population size
pbar= ProgressBar(samplepoints)#setting up the progessbar based on number of sample points
results_file = open('../../Data/large-model/Low/iter0model_output.csv', 'w')
writer = csv.writer(results_file, delimiter = ",")
enzyme_file = open('../../Data/large-model/Low/iter0enzyme_evolution.csv', 'w')
writer2 = csv.writer(enzyme_file, delimiter = ",")
writer.writerow(["Time","Bacteria_1","Bacteria_2","Bacteria_3","Bacteria_4","Bacteria_5","Bacteria_6","Bacteria_7","Bacteria_8","Bacteria_9","Bacteria_10","Bacteria_11","Bacteria_12","Bacteria_13","Bacteria_14","Bacteria_15","Bacteria_16","Bacteria_17","Bacteria_18","Bacteria_19","Bacteria_20","Bacteria_21","Bacteria_22","Bacteria_23","Bacteria_24","Bacteria_25","Bacteria_26","Bacteria_27","Bacteria_28","Bacteria_29","Bacteria_30","Bacteria_31","Bacteria_32","Bacteria_33","Bacteria_34","Bacteria_35","Bacteria_36","Bacteria_37","Bacteria_38","Bacteria_39","Bacteria_40","Bacteria_41","Bacteria_42","Bacteria_43","Bacteria_44","Bacteria_45","Bacteria_46","Bacteria_47","Bacteria_48","Bacteria_49","Bacteria_50", "Substrate_1", "Substrate_2", "Substrate_3", "Substrate_4", "Substrate_5", "Toxin"])
writer2.writerow(["Time","Bac1_Sub1","Bac1_Sub2","Bac1_Sub3","Bac1_Sub4","Bac1_Sub5","Bac2_Sub1","Bac2_Sub2","Bac2_Sub3","Bac2_Sub4","Bac2_Sub5","Bac3_Sub1","Bac3_Sub2","Bac3_Sub3","Bac3_Sub4","Bac3_Sub5","Bac4_Sub1","Bac4_Sub2","Bac4_Sub3","Bac4_Sub4","Bac4_Sub5","Bac5_Sub1","Bac5_Sub2","Bac5_Sub3","Bac5_Sub4","Bac5_Sub5","Bac6_Sub1","Bac6_Sub2","Bac6_Sub3","Bac6_Sub4","Bac6_Sub5","Bac7_Sub1","Bac7_Sub2","Bac7_Sub3","Bac7_Sub4","Bac7_Sub5","Bac8_Sub1","Bac8_Sub2","Bac8_Sub3","Bac8_Sub4","Bac8_Sub5","Bac9_Sub1","Bac9_Sub2","Bac9_Sub3","Bac9_Sub4","Bac9_Sub5","Bac10_Sub1","Bac10_Sub2","Bac10_Sub3","Bac10_Sub4","Bac10_Sub5","Bac11_Sub1","Bac11_Sub2","Bac11_Sub3","Bac11_Sub4","Bac11_Sub5","Bac12_Sub1","Bac12_Sub2","Bac12_Sub3","Bac12_Sub4","Bac12_Sub5","Bac13_Sub1","Bac13_Sub2","Bac13_Sub3","Bac13_Sub4","Bac13_Sub5","Bac14_Sub1","Bac14_Sub2","Bac14_Sub3","Bac14_Sub4","Bac14_Sub5","Bac15_Sub1","Bac15_Sub2","Bac15_Sub3","Bac15_Sub4","Bac15_Sub5","Bac16_Sub1","Bac16_Sub2","Bac16_Sub3","Bac16_Sub4","Bac16_Sub5","Bac17_Sub1","Bac17_Sub2","Bac17_Sub3","Bac17_Sub4","Bac17_Sub5","Bac18_Sub1","Bac18_Sub2","Bac18_Sub3","Bac18_Sub4","Bac18_Sub5","Bac19_Sub1","Bac19_Sub2","Bac19_Sub3","Bac19_Sub4","Bac19_Sub5","Bac20_Sub1","Bac20_Sub2","Bac20_Sub3","Bac20_Sub4","Bac20_Sub5","Bac21_Sub1","Bac21_Sub2","Bac21_Sub3","Bac21_Sub4","Bac21_Sub5","Bac22_Sub1","Bac22_Sub2","Bac22_Sub3","Bac22_Sub4","Bac22_Sub5","Bac23_Sub1","Bac23_Sub2","Bac23_Sub3","Bac23_Sub4","Bac23_Sub5","Bac24_Sub1","Bac24_Sub2","Bac24_Sub3","Bac24_Sub4","Bac24_Sub5","Bac25_Sub1","Bac25_Sub2","Bac25_Sub3","Bac25_Sub4","Bac25_Sub5","Bac26_Sub1","Bac26_Sub2","Bac26_Sub3","Bac26_Sub4","Bac26_Sub5","Bac27_Sub1","Bac27_Sub2","Bac27_Sub3","Bac27_Sub4","Bac27_Sub5","Bac28_Sub1","Bac28_Sub2","Bac28_Sub3","Bac28_Sub4","Bac28_Sub5","Bac29_Sub1","Bac29_Sub2","Bac29_Sub3","Bac29_Sub4","Bac29_Sub5","Bac30_Sub1","Bac30_Sub2","Bac30_Sub3","Bac30_Sub4","Bac30_Sub5","Bac31_Sub1","Bac31_Sub2","Bac31_Sub3","Bac31_Sub4","Bac31_Sub5","Bac32_Sub1","Bac32_Sub2","Bac32_Sub3","Bac32_Sub4","Bac32_Sub5","Bac33_Sub1","Bac33_Sub2","Bac33_Sub3","Bac33_Sub4","Bac33_Sub5","Bac34_Sub1","Bac34_Sub2","Bac34_Sub3","Bac34_Sub4","Bac34_Sub5","Bac35_Sub1","Bac35_Sub2","Bac35_Sub3","Bac35_Sub4","Bac35_Sub5","Bac36_Sub1","Bac36_Sub2","Bac36_Sub3","Bac36_Sub4","Bac36_Sub5","Bac37_Sub1","Bac37_Sub2","Bac37_Sub3","Bac37_Sub4","Bac37_Sub5","Bac38_Sub1","Bac38_Sub2","Bac38_Sub3","Bac38_Sub4","Bac38_Sub5","Bac39_Sub1","Bac39_Sub2","Bac39_Sub3","Bac39_Sub4","Bac39_Sub5","Bac40_Sub1","Bac40_Sub2","Bac40_Sub3","Bac40_Sub4","Bac40_Sub5","Bac41_Sub1","Bac41_Sub2","Bac41_Sub3","Bac41_Sub4","Bac41_Sub5","Bac42_Sub1","Bac42_Sub2","Bac42_Sub3","Bac42_Sub4","Bac42_Sub5","Bac43_Sub1","Bac43_Sub2","Bac43_Sub3","Bac43_Sub4","Bac43_Sub5","Bac44_Sub1","Bac44_Sub2","Bac44_Sub3","Bac44_Sub4","Bac44_Sub5","Bac45_Sub1","Bac45_Sub2","Bac45_Sub3","Bac45_Sub4","Bac45_Sub5","Bac46_Sub1","Bac46_Sub2","Bac46_Sub3","Bac46_Sub4","Bac46_Sub5","Bac47_Sub1","Bac47_Sub2","Bac47_Sub3","Bac47_Sub4","Bac47_Sub5","Bac48_Sub1","Bac48_Sub2","Bac48_Sub3","Bac48_Sub4","Bac48_Sub5","Bac49_Sub1","Bac49_Sub2","Bac49_Sub3","Bac49_Sub4","Bac49_Sub5","Bac50_Sub1","Bac50_Sub2","Bac50_Sub3","Bac50_Sub4","Bac50_Sub5"])
antitoxin_file = open('../../Data/large-model/Low/iter0antitoxin_evolution.csv','w')
writer3= csv.writer(antitoxin_file, delimiter = ",")
writer3.writerow(["Time","variable","value"])
N1=bacteria_start
N2=bacteria_start
N3=bacteria_start
N4=bacteria_start
N5=bacteria_start
N6=bacteria_start
N7=bacteria_start
N8=bacteria_start
N9=bacteria_start
N10=bacteria_start
N11=bacteria_start
N12=bacteria_start
N13=bacteria_start
N14=bacteria_start
N15=bacteria_start
N16=bacteria_start
N17=bacteria_start
N18=bacteria_start
N19=bacteria_start
N20=bacteria_start
N21=bacteria_start
N22=bacteria_start
N23=bacteria_start
N24=bacteria_start
N25=bacteria_start
N26=bacteria_start
N27=bacteria_start
N28=bacteria_start
N29=bacteria_start
N30=bacteria_start
N31=bacteria_start
N32=bacteria_start
N33=bacteria_start
N34=bacteria_start
N35=bacteria_start
N36=bacteria_start
N37=bacteria_start
N38=bacteria_start
N39=bacteria_start
N40=bacteria_start
N41=bacteria_start
N42=bacteria_start
N43=bacteria_start
N44=bacteria_start
N45=bacteria_start
N46=bacteria_start
N47=bacteria_start
N48=bacteria_start
N49=bacteria_start
N50=bacteria_start
writer.writerow([0,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50,S1,S2,S3,S4,S5,toxin])


for i in pbar(samplepoints):
	#for each of the sample points call each bacterial equation once and write the output to the results file
	bacteria_1_output = bacteria_1(N1,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_1_output < 0.0001):
		N1=0
	elif (bacteria_1_output >10):
		N1=10
	else:
		N1 = bacteria_1_output

	bacteria_2_output = bacteria_2(N2,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_2_output < 0.0001):
		N2=0
	elif (bacteria_2_output >10):
		N2=10
	else:
		N2 = bacteria_2_output

	bacteria_3_output = bacteria_3(N3,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_3_output < 0.0001):
		N3=0
	elif (bacteria_3_output >10):
		N3=10
	else:
		N3 = bacteria_3_output

	bacteria_4_output = bacteria_4(N4,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_4_output < 0.0001):
		N4=0
	elif (bacteria_4_output >10):
		N4=10
	else:
		N4 = bacteria_4_output

	bacteria_5_output = bacteria_5(N5,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_5_output < 0.0001):
		N5=0
	elif (bacteria_5_output >10):
		N5=10
	else:
		N5 = bacteria_5_output

	bacteria_6_output = bacteria_6(N6,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_6_output < 0.0001):
		N6=0
	elif (bacteria_6_output >10):
		N6=10
	else:
		N6 = bacteria_6_output

	bacteria_7_output = bacteria_7(N7,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_7_output < 0.0001):
		N7=0
	elif (bacteria_7_output >10):
		N7=10
	else:
		N7 = bacteria_7_output

	bacteria_8_output = bacteria_8(N8,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_8_output < 0.0001):
		N8=0
	elif (bacteria_8_output >10):
		N8=10
	else:
		N8 = bacteria_8_output

	bacteria_9_output = bacteria_9(N9,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_9_output < 0.0001):
		N9=0
	elif (bacteria_9_output >10):
		N9=10
	else:
		N9 = bacteria_9_output

	bacteria_10_output = bacteria_10(N10,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_10_output < 0.0001):
		N10=0
	elif (bacteria_10_output >10):
		N10=10
	else:
		N10 = bacteria_10_output

	bacteria_11_output = bacteria_11(N11,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_11_output < 0.0001):
		N11=0
	elif (bacteria_11_output >10):
		N11=10
	else:
		N11 = bacteria_11_output

	bacteria_12_output = bacteria_12(N12,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_12_output < 0.0001):
		N12=0
	elif (bacteria_12_output >10):
		N12=10
	else:
		N12 = bacteria_12_output

	bacteria_13_output = bacteria_13(N13,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_13_output < 0.0001):
		N13=0
	elif (bacteria_13_output >10):
		N13=10
	else:
		N13 = bacteria_13_output

	bacteria_14_output = bacteria_14(N14,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_14_output < 0.0001):
		N14=0
	elif (bacteria_14_output >10):
		N14=10
	else:
		N14 = bacteria_14_output

	bacteria_15_output = bacteria_15(N15,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_15_output < 0.0001):
		N15=0
	elif (bacteria_15_output >10):
		N15=10
	else:
		N15 = bacteria_15_output

	bacteria_16_output = bacteria_16(N16,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_16_output < 0.0001):
		N16=0
	elif (bacteria_16_output >10):
		N16=10
	else:
		N16 = bacteria_16_output

	bacteria_17_output = bacteria_17(N17,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_17_output < 0.0001):
		N17=0
	elif (bacteria_17_output >10):
		N17=10
	else:
		N17 = bacteria_17_output

	bacteria_18_output = bacteria_18(N18,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_18_output < 0.0001):
		N18=0
	elif (bacteria_18_output >10):
		N18=10
	else:
		N18 = bacteria_18_output

	bacteria_19_output = bacteria_19(N19,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_19_output < 0.0001):
		N19=0
	elif (bacteria_19_output >10):
		N19=10
	else:
		N19 = bacteria_19_output

	bacteria_20_output = bacteria_20(N20,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_20_output < 0.0001):
		N20=0
	elif (bacteria_20_output >10):
		N20=10
	else:
		N20 = bacteria_20_output

	bacteria_21_output = bacteria_21(N21,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_21_output < 0.0001):
		N21=0
	elif (bacteria_21_output >10):
		N21=10
	else:
		N21 = bacteria_21_output

	bacteria_22_output = bacteria_22(N22,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_22_output < 0.0001):
		N22=0
	elif (bacteria_22_output >10):
		N22=10
	else:
		N22 = bacteria_22_output

	bacteria_23_output = bacteria_23(N23,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_23_output < 0.0001):
		N23=0
	elif (bacteria_23_output >10):
		N23=10
	else:
		N23 = bacteria_23_output

	bacteria_24_output = bacteria_24(N24,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_24_output < 0.0001):
		N24=0
	elif (bacteria_24_output >10):
		N24=10
	else:
		N24 = bacteria_24_output

	bacteria_25_output = bacteria_25(N25,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_25_output < 0.0001):
		N25=0
	elif (bacteria_25_output >10):
		N25=10
	else:
		N25 = bacteria_25_output

	bacteria_26_output = bacteria_26(N26,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_26_output < 0.0001):
		N26=0
	elif (bacteria_26_output >10):
		N26=10
	else:
		N26 = bacteria_26_output

	bacteria_27_output = bacteria_27(N27,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_27_output < 0.0001):
		N27=0
	elif (bacteria_27_output >10):
		N27=10
	else:
		N27 = bacteria_27_output

	bacteria_28_output = bacteria_28(N28,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_28_output < 0.0001):
		N28=0
	elif (bacteria_28_output >10):
		N28=10
	else:
		N28 = bacteria_28_output

	bacteria_29_output = bacteria_29(N29,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_29_output < 0.0001):
		N29=0
	elif (bacteria_29_output >10):
		N29=10
	else:
		N29 = bacteria_29_output

	bacteria_30_output = bacteria_30(N30,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_30_output < 0.0001):
		N30=0
	elif (bacteria_30_output >10):
		N30=10
	else:
		N30 = bacteria_30_output

	bacteria_31_output = bacteria_31(N31,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_31_output < 0.0001):
		N31=0
	elif (bacteria_31_output >10):
		N31=10
	else:
		N31 = bacteria_31_output

	bacteria_32_output = bacteria_32(N32,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_32_output < 0.0001):
		N32=0
	elif (bacteria_32_output >10):
		N32=10
	else:
		N32 = bacteria_32_output

	bacteria_33_output = bacteria_33(N33,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_33_output < 0.0001):
		N33=0
	elif (bacteria_33_output >10):
		N33=10
	else:
		N33 = bacteria_33_output

	bacteria_34_output = bacteria_34(N34,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_34_output < 0.0001):
		N34=0
	elif (bacteria_34_output >10):
		N34=10
	else:
		N34 = bacteria_34_output

	bacteria_35_output = bacteria_35(N35,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_35_output < 0.0001):
		N35=0
	elif (bacteria_35_output >10):
		N35=10
	else:
		N35 = bacteria_35_output

	bacteria_36_output = bacteria_36(N36,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_36_output < 0.0001):
		N36=0
	elif (bacteria_36_output >10):
		N36=10
	else:
		N36 = bacteria_36_output

	bacteria_37_output = bacteria_37(N37,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_37_output < 0.0001):
		N37=0
	elif (bacteria_37_output >10):
		N37=10
	else:
		N37 = bacteria_37_output

	bacteria_38_output = bacteria_38(N38,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_38_output < 0.0001):
		N38=0
	elif (bacteria_38_output >10):
		N38=10
	else:
		N38 = bacteria_38_output

	bacteria_39_output = bacteria_39(N39,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_39_output < 0.0001):
		N39=0
	elif (bacteria_39_output >10):
		N39=10
	else:
		N39 = bacteria_39_output

	bacteria_40_output = bacteria_40(N40,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_40_output < 0.0001):
		N40=0
	elif (bacteria_40_output >10):
		N40=10
	else:
		N40 = bacteria_40_output

	bacteria_41_output = bacteria_41(N41,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_41_output < 0.0001):
		N41=0
	elif (bacteria_41_output >10):
		N41=10
	else:
		N41 = bacteria_41_output

	bacteria_42_output = bacteria_42(N42,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_42_output < 0.0001):
		N42=0
	elif (bacteria_42_output >10):
		N42=10
	else:
		N42 = bacteria_42_output

	bacteria_43_output = bacteria_43(N43,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_43_output < 0.0001):
		N43=0
	elif (bacteria_43_output >10):
		N43=10
	else:
		N43 = bacteria_43_output

	bacteria_44_output = bacteria_44(N44,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_44_output < 0.0001):
		N44=0
	elif (bacteria_44_output >10):
		N44=10
	else:
		N44 = bacteria_44_output

	bacteria_45_output = bacteria_45(N45,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_45_output < 0.0001):
		N45=0
	elif (bacteria_45_output >10):
		N45=10
	else:
		N45 = bacteria_45_output

	bacteria_46_output = bacteria_46(N46,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_46_output < 0.0001):
		N46=0
	elif (bacteria_46_output >10):
		N46=10
	else:
		N46 = bacteria_46_output

	bacteria_47_output = bacteria_47(N47,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_47_output < 0.0001):
		N47=0
	elif (bacteria_47_output >10):
		N47=10
	else:
		N47 = bacteria_47_output

	bacteria_48_output = bacteria_48(N48,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_48_output < 0.0001):
		N48=0
	elif (bacteria_48_output >10):
		N48=10
	else:
		N48 = bacteria_48_output

	bacteria_49_output = bacteria_49(N49,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_49_output < 0.0001):
		N49=0
	elif (bacteria_49_output >10):
		N49=10
	else:
		N49 = bacteria_49_output

	bacteria_50_output = bacteria_50(N50,kcat,conv,E,tox,dilution,toxin,antitoxin,S1,S2,S3,S4,S5)
	if (bacteria_50_output < 0.0001):
		N50=0
	elif (bacteria_50_output >10):
		N50=10
	else:
		N50 = bacteria_50_output

	new_substrate_1= substrate_1(S1,kcat,km,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50)
	if (new_substrate_1 < 0.0001):
		S1=0
	else:
		S1=new_substrate_1


	new_substrate_2= substrate_2(S2,kcat,km,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50)
	if (new_substrate_2 < 0.0001):
		S2=0
	else:
		S2=new_substrate_2


	new_substrate_3= substrate_3(S3,kcat,km,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50)
	if (new_substrate_3 < 0.0001):
		S3=0
	else:
		S3=new_substrate_3


	new_substrate_4= substrate_4(S4,kcat,km,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50)
	if (new_substrate_4 < 0.0001):
		S4=0
	else:
		S4=new_substrate_4


	new_substrate_5= substrate_5(S5,kcat,km,E,dilution,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50)
	if (new_substrate_5 < 0.0001):
		S5=0
	else:
		S5=new_substrate_5


	if i in range(3000, 5000):
		new_toxin=toxin_eqn(toxin,toxin_1)
		toxin= new_toxin


	else:
		new_toxin=toxin_eqn(toxin,toxin_0)
		toxin= new_toxin

	S=[S1,S2,S3,S4,S5] #forms a matrix of the substrate values at the end of that run

### THE ENZYME EVOLUTION CODE ###

	for bacteria_ID in range(bacteria_num):
		if (i%5 == 0):
			N=[N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50] #forms a matrix of the substrate values at the end of that run
			if N[bacteria_ID] > 0:
				mu = [random.uniform(0, 0.02) for x in range(substrate_num)]
				mut = random.uniform(0, 0.02)
				a=LpVariable('a',0,1)
				b=LpVariable('b',0,1)
				c=LpVariable('c',0,1)
				d=LpVariable('d',0,1)
				e=LpVariable('e',0,1)
				anti = LpVariable('anti',0,1)
				prob = LpProblem("myProblem",LpMaximize)
				prob += a+b+c+d+e+anti<=1 #the constraint that they must all sum to 1
				prob+= (conv[bacteria_ID][0]*(kcat[bacteria_ID][0]*a*S1/(km[bacteria_ID][0]+S1)))+(conv[bacteria_ID][1]*(kcat[bacteria_ID][1]*b*S2/(km[bacteria_ID][1]+S2)))+(conv[bacteria_ID][2]*(kcat[bacteria_ID][2]*c*S3/(km[bacteria_ID][2]+S3)))+(conv[bacteria_ID][3]*(kcat[bacteria_ID][3]*d*S4/(km[bacteria_ID][3]+S4)))+(conv[bacteria_ID][4]*(kcat[bacteria_ID][4]*e*S5/(km[bacteria_ID][4]+S5)))+(kcat[bacteria_ID][5]*anti*toxin/(km[bacteria_ID][5]+toxin))

				status = prob.solve()
				LpStatus[status]

				best_enzyme= [int(value(a)),int(value(b)),int(value(c)),int(value(d)),int(value(e))]
				E_diff = numpy.array(E[bacteria_ID])-numpy.array(best_enzyme)
				E_diff_matrix = E_diff*mu
				E[bacteria_ID] = E[bacteria_ID] - E_diff_matrix

				best_antitoxin=int(value(anti))
				antitoxin_diff = best_antitoxin-antitoxin[bacteria_ID]
				new_antitoxin = antitoxin_diff*mut
				antitoxin[bacteria_ID] = antitoxin[bacteria_ID]+ new_antitoxin
				writer3.writerow([i+1,"Bac"+str(bacteria_ID+1),antitoxin[bacteria_ID]])

			elif N[bacteria_ID] == 0 :
				E[bacteria_ID] = E[bacteria_ID] * 0
				antitoxin[bacteria_ID] = antitoxin[bacteria_ID] * 0

	if i % timestep == 0:
		writer2.writerow([i+1,E[0][0],E[0][1],E[0][2],E[0][3],E[0][4],E[1][0],E[1][1],E[1][2],E[1][3],E[1][4],E[2][0],E[2][1],E[2][2],E[2][3],E[2][4],E[3][0],E[3][1],E[3][2],E[3][3],E[3][4],E[4][0],E[4][1],E[4][2],E[4][3],E[4][4],E[5][0],E[5][1],E[5][2],E[5][3],E[5][4],E[6][0],E[6][1],E[6][2],E[6][3],E[6][4],E[7][0],E[7][1],E[7][2],E[7][3],E[7][4],E[8][0],E[8][1],E[8][2],E[8][3],E[8][4],E[9][0],E[9][1],E[9][2],E[9][3],E[9][4],E[10][0],E[10][1],E[10][2],E[10][3],E[10][4],E[11][0],E[11][1],E[11][2],E[11][3],E[11][4],E[12][0],E[12][1],E[12][2],E[12][3],E[12][4],E[13][0],E[13][1],E[13][2],E[13][3],E[13][4],E[14][0],E[14][1],E[14][2],E[14][3],E[14][4],E[15][0],E[15][1],E[15][2],E[15][3],E[15][4],E[16][0],E[16][1],E[16][2],E[16][3],E[16][4],E[17][0],E[17][1],E[17][2],E[17][3],E[17][4],E[18][0],E[18][1],E[18][2],E[18][3],E[18][4],E[19][0],E[19][1],E[19][2],E[19][3],E[19][4],E[20][0],E[20][1],E[20][2],E[20][3],E[20][4],E[21][0],E[21][1],E[21][2],E[21][3],E[21][4],E[22][0],E[22][1],E[22][2],E[22][3],E[22][4],E[23][0],E[23][1],E[23][2],E[23][3],E[23][4],E[24][0],E[24][1],E[24][2],E[24][3],E[24][4],E[25][0],E[25][1],E[25][2],E[25][3],E[25][4],E[26][0],E[26][1],E[26][2],E[26][3],E[26][4],E[27][0],E[27][1],E[27][2],E[27][3],E[27][4],E[28][0],E[28][1],E[28][2],E[28][3],E[28][4],E[29][0],E[29][1],E[29][2],E[29][3],E[29][4],E[30][0],E[30][1],E[30][2],E[30][3],E[30][4],E[31][0],E[31][1],E[31][2],E[31][3],E[31][4],E[32][0],E[32][1],E[32][2],E[32][3],E[32][4],E[33][0],E[33][1],E[33][2],E[33][3],E[33][4],E[34][0],E[34][1],E[34][2],E[34][3],E[34][4],E[35][0],E[35][1],E[35][2],E[35][3],E[35][4],E[36][0],E[36][1],E[36][2],E[36][3],E[36][4],E[37][0],E[37][1],E[37][2],E[37][3],E[37][4],E[38][0],E[38][1],E[38][2],E[38][3],E[38][4],E[39][0],E[39][1],E[39][2],E[39][3],E[39][4],E[40][0],E[40][1],E[40][2],E[40][3],E[40][4],E[41][0],E[41][1],E[41][2],E[41][3],E[41][4],E[42][0],E[42][1],E[42][2],E[42][3],E[42][4],E[43][0],E[43][1],E[43][2],E[43][3],E[43][4],E[44][0],E[44][1],E[44][2],E[44][3],E[44][4],E[45][0],E[45][1],E[45][2],E[45][3],E[45][4],E[46][0],E[46][1],E[46][2],E[46][3],E[46][4],E[47][0],E[47][1],E[47][2],E[47][3],E[47][4],E[48][0],E[48][1],E[48][2],E[48][3],E[48][4],E[49][0],E[49][1],E[49][2],E[49][3],E[49][4]])
		writer.writerow([i+1,N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50,S1,S2,S3,S4,S5,toxin])

print ("Model run successfully")

results_file.close()
enzyme_file.close()
antitoxin_file.close()


bac_tuples = [("N1",N1),("N2",N2),("N3",N3),("N4",N4),("N5",N5),("N6",N6),("N7",N7),("N8",N8),("N9",N9),("N10",N10),("N11",N11),("N12",N12),("N13",N13),("N14",N14),("N15",N15),("N16",N16),("N17",N17),("N18",N18),("N19",N19),("N20",N20),("N21",N21),("N22",N22),("N23",N23),("N24",N24),("N25",N25),("N26",N26),("N27",N27),("N28",N28),("N29",N29),("N30",N30),("N31",N31),("N32",N32),("N33",N33),("N34",N34),("N35",N35),("N36",N36),("N37",N37),("N38",N38),("N39",N39),("N40",N40),("N41",N41),("N42",N42),("N43",N43),("N44",N44),("N45",N45),("N46",N46),("N47",N47),("N48",N48),("N49",N49),("N50",N50)]
sorted_bacs= sorted(bac_tuples,key = lambda bac: bac[1])
highest= sorted_bacs[49][0][1:]
lowest= sorted_bacs[0][0][1:]
second_highest= sorted_bacs[48][0][1:]
N=[N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20,N21,N22,N23,N24,N25,N26,N27,N28,N29,N30,N31,N32,N33,N34,N35,N36,N37,N38,N39,N40,N41,N42,N43,N44,N45,N46,N47,N48,N49,N50] #forms a matrix of the substrate values at the end of that run

diagnostics_file = open("../../Data/large-model/Low/iter0diagnostics-groups"+str(functional_groups)+".txt","w")
diagnostics_file.write("This is large-low-iter0")
diagnostics_file.write("\nNumber of functional groups is: "+str(functional_groups))
diagnostics_file.write("\nHighest bacteria is N "+ highest)
diagnostics_file.write("\npopulation size: "+str(N[int(highest)-1]))
diagnostics_file.write("\nWith enzyme kinetics:")
diagnostics_file.write("\n\nconversion:"+str(conv[int(highest)-1]))
diagnostics_file.write("\nkcat: "+str(kcat[int(highest)-1]))
diagnostics_file.write("\nkm: "+str(km[int(highest)-1]))
diagnostics_file.write("\nE: "+str(E[int(highest)-1]))
diagnostics_file.write("\ntoxicity: "+str(tox[int(highest)-1]))

diagnostics_file.write("\n\nNext highest bacteria is N "+ second_highest)
diagnostics_file.write("\npopulation size: "+str(N[int(second_highest)-1]))
diagnostics_file.write("\nWith enzyme kinetics:")
diagnostics_file.write("\n\nconversion:"+str(conv[int(second_highest)-1]))
diagnostics_file.write("\nkcat: "+str(kcat[int(second_highest)-1]))
diagnostics_file.write("\nkm: "+str(km[int(second_highest)-1]))
diagnostics_file.write("\nE: "+str(E[int(second_highest)-1]))
diagnostics_file.write("\ntoxicity: "+str(tox[int(second_highest)-1]))
diagnostics_file.write("\n\nwhole ending population is: "+str(bac_tuples))
diagnostics_file.write("\nwhole ending enzyme allocation is: \n"+str(E))

diagnostics_file.close()